#!/usr/bin/env python

import os
import sys
import optparse
import subprocess
import util
import logging
logger = util.getlogger('INRS.IEHSS.Fortran')

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
class CallXtrcmd:

    def __init__(self, opts):
        self.opts = opts
        self._def_common_args()

    def _def_common_args(self):
        inrs_dev = os.environ['INRS_DEV']
        xtrcmd = util.getpath(inrs_dev, 'tools', 'xtrcmd', 'xtrcmd.py')
        inc = '@%s' % util.getpath(inrs_dev, 'H2D2', 'h2d2.i')
        self.base_args = ['python', xtrcmd, inc, '-f', self.opts.format]

    def __call__(self, for_file, prj_dir):

        mdl = 'module=%s' % os.path.basename(prj_dir)
        out = os.path.basename(for_file)
        out = os.path.splitext(out)[0]
        if self.opts.format == 'html':
            out += '.html'
        else:
            out += '.hlp'
        out = os.path.join(prj_dir, 'doc', out)
        tmp = out + '.new'

        args = self.base_args + ['-k', mdl, '-o', tmp, for_file]
        logger.debug('calling: %s' % str(args))
        if subprocess.call(args) != 0:
            raise util.CIError('xtrcmd.py failed')

        util.update_file(out, tmp, logger)


#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def process_args(args):

    parser = optparse.OptionParser()
    parser.set_usage('Usage: %prog [options] [directories]')

    formats = ['html', 'text']
    parser.add_option('-f', '--format', dest='format', default='html',
                      choices=formats, help='output format : %s' % formats)

    opts, args = parser.parse_args(args)

    for a in args:
        a = os.path.abspath(a)
        if not os.path.isdir(a):
            parser.error('invalid directory: %s' % a)
    if not args:
        args.append(os.environ['INRS_DEV'])

    return opts, args

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def main(args=None):

    if not args:
        args = sys.argv[1:]
    opts, args = process_args(args)

    call_xtrcmd = CallXtrcmd(opts)
    required = ['build', 'prjVisual', 'source']
    skipped = ['proto']
    zero_successful = True
    for a in args:
        for d in util.get_valid_dirs(a, required, skipped):
            for f in util.get_valid_files(d, r'.*\.for'):
                try:
                    call_xtrcmd(f, d)
                    zero_successful = False
                except util.CIError, msg:
                    logger.warning(msg)
    if zero_successful:
        raise util.CIError('every calls failed')


try:
    util.check_env()
    if __name__ == '__main__':
        main()
except util.CIError, msg:
    logger.error(msg)
    sys.exit(1)

