#!/usr/bin/env python

import os
import sys
import optparse
import subprocess
import util
import logging
logger = util.getlogger('INRS.IEHSS.compil')

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def call_scons(opts):
    sconstruct = util.getpath(os.environ['INRS_BLD'], 'MUC_SCons', 'SConstruct')
    sconstruct = '--file=%s' % sconstruct
    topdir = '"topdir=%s"' % util.getpath(os.environ['INRS_DEV'], 'H2D2-RemailleurH2D2')
    try:
        mpi = '"mpilibs=%s"' % os.environ['MPI']
        compiler = '"compilers=%s"' % os.environ['COMPILER']
    except KeyError, key:
        raise util.CIError('%s undefined' % key)
    args = ' '.join(['scons -Q', sconstruct, topdir, mpi, compiler])

    build = [('"builds=debug"', 'debug'), ('"builds=release"', 'release')]
    link  = [('', 'dynamic'), ('"forcestatic=on"', 'static')]
    state = []
    for bld in build:
        for lnk in link:
            cmd = ' '.join([args, bld[0], lnk[0]])
            logger.info('--- compiling: %s %s ---' % (lnk[1], bld[1]))
            if opts.verbose:
                ret = subprocess.call(cmd, shell=True)
            else:
                ret = subprocess.call(cmd, shell=True, stdout=open(os.devnull, 'w'))
            if ret:
                logger.error('scons failed')
                state.append('fail')
            else:
                state.append('success')
    for i, bld in enumerate(build):
        for j, lnk in enumerate(link):
            index = j + 2 * i
            logger.info('%s %s: %s' % (lnk[1], bld[1], state[index]))

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def process_args(args):
    parser = optparse.OptionParser()
    parser.add_option('-v', '--verbose', action='store_true', dest='verbose', default=False,
                      help='display more status messages')
    parser.add_option('-s', '--server', help='web server name or address')
    parser.add_option('-p', '--port',   help='web server ssh port')
    parser.add_option('-u', '--user',   help='web server ssh user')
    parser.add_option('-b', '--branch', help='the H2D2 branch, if not HEAD', default='HEAD')
    opts, args = parser.parse_args(args)
    if args:
        parser.error('this script does not expect any argument')
    return opts

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def main(args=None):
    if not args:
        args = sys.argv[1:]
    opts = process_args(args)

    call_scons(opts)
    if opts.server:
        dest = ''
        if opts.user: dest = '%s@' % opts.user
        dest += '%s:CI_global_results/' % opts.server
        dest += opts.branch
        try:
            util.rsync('_*', dest, excludes=['__mod__'], port=opts.port)
        except util.CIError, msg:
            logger.warning(msg)


try:
    util.check_env()
    if __name__ == '__main__':
        main()
except util.CIError, msg:
    logger.error(msg)
    sys.exit(1)

