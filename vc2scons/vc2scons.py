#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import os
import traceback

from vcproj import MSVCSolution, MSVCProject
from config import Config
from param  import Parametres
from scons  import Scons
from setup  import CygwinSetup

def main(args):
    nomPrj = ""
    nomSol = ""
    sol = MSVCSolution()
    prj = None # MSVCProject()
    ret = 0

    LOGGER.info(Config.nomLogiciel + "    " + Config.versionLogiciel)

    prms = Parametres()
    prms.parseParamEntree(args)
    if not prms.valide: return

    try:
        # ---  Lis la solution ou le projet
        nomInp = prms.ficPrjVisual
        if not os.path.isfile(nomInp):
            raise RuntimeError("File not found: %s", nomInp)
        ext = os.path.splitext(nomInp)[1]
        if ext == ".sln":
            LOGGER.info("Reading VS solution: " + nomInp)
            nomSol = nomInp
            sol.loadSolution(nomSol)
        elif ext == ".vcxproj":
            LOGGER.info("Reading VS project: " + nomInp)
            nomPrj = nomInp
            prj.loadProjet(nomPrj)
        else:
            LOGGER.error("Invalid solution or project file: %s" % nomInp)
            raise RuntimeError("Invalid solution or project file: %s" % nomInp)

        # ---  Boucle sur les systèmes
        for prms.bldSystem in prms.bldSystems:
            wrtr = None
            if prms.bldSystem == "setup":
                LOGGER.info("Writing setup")
                wrtr = CygwinSetup(prms)
            elif prms.bldSystem == "scons":
                LOGGER.info("Writing scons")
                wrtr = Scons(prms)

            # ---  Traite
            if nomSol: wrtr.traiteSol(sol, *args)
            if nomPrj: wrtr.traitePrj(prj, *args)

    except Exception as e:
        errMsg = "Exception\n%s\n%s" % (str(e), traceback.format_exc())
        LOGGER.error(errMsg)
        LOGGER.info("Executing: %s", Config.nomLogiciel)
        for arg in args:
            LOGGER.info("  %s", arg)
        ret = -1

    return ret

if __name__ == "__main__":
    import sys
    from addLogLevel import addLoggingLevel

    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    strHndlr = logging.StreamHandler()
    FORMAT = "[%(levelname)-8.8s] %(message)s"
    strHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER = logging.getLogger("vc2scons")
    LOGGER.addHandler(strHndlr)
    LOGGER.setLevel(logging.INFO)

    # mode_test = True
    mode_test = False
    if mode_test:
        #mdl = 'py_h2d2_umef'
        #args = [
        #    r'-input_project_file=pyH2D2/%s/prjVisual/%s.sln' % (mdl, mdl),
        #    r'-output_group_name=pyH2D2',
        #    r'-output_dir=pyH2D2/%s' % mdl,
        #    r'-output_systems=scons;',
        #    r'-project_root=E:/inrs-dev',
        #    r'-no_stage_target',
        #    ]
        INRS_DEV = os.environ['INRS_DEV']
        args = [
            r"%s/build/vc2scons/vc2scons.py" % INRS_DEV,
            r"-input_project_file=%s/H2D2-tools/prjVisual/h2d2_tools_python.sln" % INRS_DEV,
            r"-output_file_name_format='{sys_base}_{target}{sys_ext}'",
            r"-output_group_name=H2D2-tools",
            r"-output_dir=%s/H2D2-tools" % INRS_DEV,
            r"-output_systems=scons;",
            r"-project_root=%s" % INRS_DEV,
            r"-no_stage_target",
        ]
    else:
        args = sys.argv

    main(args)
