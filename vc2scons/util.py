#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import os

LOGGER = logging.getLogger("vc2scons.util")

def path2unix(p):
    return p.replace('\\', '/')

def ajouteVersion(path, prms):
    """
    Ajoute la version au chemin
    """
    ret = "/" + path
    for k, v in prms.versions.items():
        find = "/" + k + "/"
        repl = "/" + k + "/" + v + "/"
        ret = ret.replace(find, repl)
    ret = ret[1:]

    ret = "\\" + ret
    for k, v in prms.versions.items():
        find = "\\" + k + "\\"
        repl = "\\" + k + "\\" + v + "\\"
        ret = ret.replace(find, repl)
    ret = ret[1:]

    return ret

#    '--------------------------------------------------------------------
#    '  Enlève la version au chemin
#    '--------------------------------------------------------------------
#    Function enleveVersion(ByRef path)
#
#        Dim ret = path
#        Dim i As Integer
#
#        ret = "/" & ret
#        for i = 0 To vars.versions.Count - 1
#            Dim find, repl
#            find = "/" & vars.versions.GetKey(i) & "/" & vars.versions.GetByIndex(i) & "/"
#            repl = "/" & vars.versions.GetKey(i) & "/"
#            ret = Replace(ret, find, repl)
#        Next i
#        ret = Mid(ret, 2)
#
#        ret = "\" & ret
#        for i = 0 To vars.versions.Count - 1
#            Dim find, repl
#            find = "\" & vars.versions.GetKey(i) & "\" & vars.versions.GetByIndex(i) & "\"
#            repl = "\" & vars.versions.GetKey(i) & "\"
#            ret = Replace(ret, find, repl)
#        Next i
#        ret = Mid(ret, 2)
#
#        enleveVersion = ret
#    End Function

def xtrNomModule(dirProj, prjRoot):
    """
    Extrait le nom du module à partir du nom du répertoire
    """
    nomRel = ""
    # ---  Nom relatif
    try:
        nomRel = xtrNomRelatif(dirProj, prjRoot)
    except Exception as e:
        path = path2unix(dirProj)
        path = path.replace('"', '')
        #   if len(path) > 1 and path[0] == '"'): path = path[1:]
        if len(path) > 10 and path[:10] == "$HOME/dev/": path = path[11:]
        if len(path) > 10 and path[:10] == "$INRS_DEV/": path = path[11:]
        if len(path) > 12 and path[:12] == "$(INRS_DEV)/": path = path[13:]
        nomRel = path

    # ---  Enlève le répertoire de projet au bout
    if nomRel[-1] == "/": nomRel = nomRel[:-2]
    if nomRel == "prjVisual": nomRel = ""
    if nomRel == "VisualStudio": nomRel = ""
    if nomRel == "Visual Studio Solution": nomRel = ""
    if nomRel[-10:] == "/prjVisual": nomRel = nomRel[:-10]
    if nomRel[-13:] == "/VisualStudio": nomRel = nomRel[:-13]
    if nomRel[-23:] == "/Visual Studio Solution": nomRel = nomRel[:-23]
    if "/prjVisual/" in nomRel: nomRel = nomRel.split("/prjVisual/")[0]
    if "/VisualStudio/" in nomRel: nomRel = nomRel.split("/VisualStudio/")[0]
    if "/Visual Studio Solution/" in nomRel: nomRel = nomRel.split("/Visual Studio Solution/")[0]

    return nomRel

def xtrNomRelatif(nomFic, nomRep):
    """
    Extrait le nom relatif par rapport à un repertoire donné
    """
    nomFic_ = os.path.normpath(nomFic)
    if nomFic_[-1] == "\\": nomFic_ = nomFic_[:-2]
    if nomFic_[-1] == "/":  nomFic_ = nomFic_[:-2]
    nomRep_ = os.path.normpath(nomRep)
    if nomRep_[-1] == "\\": nomRep_ = nomRep_[:-2]
    if nomRep_[-1] == "/":  nomRep_ = nomRep_[:-2]

    nomFic_ = os.path.relpath(nomFic_, start=nomRep_)
    while nomFic_[:2] == ".\\":
        nomFic_ = nomFic_[3:]

    nomFic_ = path2unix(nomFic_)
    return nomFic_

def xtrPathProjet(dirProj):
    """
    Extrait le chemin du projet 
    """
    if dirProj[-10:] in ['\\prjVisual', '/prjVisual']:
        path = dirProj[:-10]
    else:
        path = dirProj
    return path2unix(path)


def remplaceEnvINRS(path):
    """
    Extrait le nom du module à partir du nom du répertoire
    """
    nomRel = path
    tok = "/lib_externes/"
    if tok in nomRel: nomRel = "$INRS_LXT/" + nomRel.split(tok, 1)[-1]
    tok = "/external/"
    if tok in nomRel: nomRel = "$INRS_LXT/" + nomRel.split(tok, 1)[-1]
    tok = "/dev/"
    if tok in nomRel: nomRel = "$INRS_DEV/" + nomRel.split(tok, 1)[-1]
    tok = "/build/"
    if tok in nomRel: nomRel = "$INRS_BLD/" + nomRel.split(tok, 1)[-1]
    return nomRel


if __name__ == "__main__":
    #addLoggingLevel('DUMP',  logging.DEBUG + 5)
    #addLoggingLevel('TRACE', logging.DEBUG - 5)

    strHndlr = logging.StreamHandler()
    FORMAT = "[%(levelname)-8.8s] %(message)s"
    strHndlr.setFormatter( logging.Formatter(FORMAT) )
    strHndlr.setLevel(logging.DEBUG)
    
    LOGGER = logging.getLogger("vc2scons")
    LOGGER.addHandler(strHndlr)
    LOGGER.setLevel(logging.DEBUG)

    args = [
        r"-input_project_file=E:\h2d2-dev\H2D2\algo_remesh\prjVisual\algo_remesh.sln",
        r"-input_setup_config_file=H2D2\algo_remesh\build\setup.cfg",
        r"-output_group_name=H2D2",
        r"-output_dir=E:\h2d2-dev\H2D2\algo_remesh",
        r"-output_systems=scons;setup;",
        r"-project_root=E:\h2d2SurGitHub",
        r"-no_stage_target",
        ]

    main(args)
