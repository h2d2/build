# -*- coding: UTF-8 -*-
# From https://github.com/cython/cython/blob/master/Tools/site_scons/site_tools/cython.py

"""
Tool to run Cython files (.pyx) into .c and .cpp.

TODO:
 - Add support for dynamically selecting in-process Cython
   through CYTHONINPROCESS variable.
 - Have a CYTHONCPP option which turns on C++ in flags and
   changes output extension at the same time

VARIABLES:
 - CYTHON - The path to the "cython" command line tool.
 - CYTHONFLAGS - Flags to pass to the "cython" command line tool.

AUTHORS:
 - David Cournapeau
 - Dag Sverre Seljebotn

"""
import SCons
from MUC_SCons.SCons.Scanner.Cython import CythonScanner, CythonSuffixes

# distutils/sysconfig.py uses _imp.extension_suffixes()[0] and not
# sysconfig.get_config_var('EXT_SUFFIX'). get_config_var is not
# reliable under Windows conda python 3.6.6
import _imp
MUC_CYTHON_EXT = _imp.extension_suffixes()[0]

cythonAction = SCons.Action.Action("$CYTHON_COM")

def cython_src_suffix_emitter(env, source):
    return "$CYTHON_SRCSUFFIX"

def cython_tgt_suffix_emitter(env, source):
    return "$CYTHON_TGTSUFFIX"

def create_builder(env):
    try:
        cython = env['BUILDERS']['Cython']
    except KeyError:
        cython = SCons.Builder.Builder(
                  action = cythonAction,
                  emitter = {},
                  suffix = cython_tgt_suffix_emitter,
                  src_suffix = cython_src_suffix_emitter,
                  single_source = 1)
        env['BUILDERS']['Cython'] = cython

    return cython

def generate(env):
    """Hook the cython builder and scanner into the environment."""
    for suffix in CythonSuffixes:
        SCons.Tool.SourceFileScanner.add_scanner(suffix, CythonScanner)

    env["CYTHON"]        = "python"
    env["CYTHON_LIBDIR"] = "${TARGET.dir.abspath}"
    env["CYTHON_TMPDIR"] = "${CYTHON_LIBDIR}/__cython-build"
    env["CYTHON_FLAGS"]  = "build_ext --build-lib=${CYTHON_LIBDIR} --build-temp=${CYTHON_TMPDIR}"
    env["CYTHON_COM"]    = "cd ${SOURCE.dir} && ${CYTHON} ${SOURCE.filebase}.setup.py ${CYTHON_FLAGS}"
    env["CYTHON_SRCSUFFIX"] = CythonSuffixes
    env["CYTHON_TGTSUFFIX"] = MUC_CYTHON_EXT

    create_builder(env)

def exists(env):
    try:
        import Cython
        return True
    except ImportError:
        return False
        
        
