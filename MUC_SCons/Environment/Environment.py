# -*- coding: UTF-8 -*-
import SCons

import ExternalLib
import Util
import MUC_platform

import collections.abc
import os
import shutil
import six
import subprocess
import sys

import scons_patches

SConsEnv = SCons.Environment.Environment

MUC_PATH_PYTHON = None
MUC_PATH_TAR    = None

pyPath = SCons.Util.WhereIs('python')
if pyPath:
    MUC_PATH_PYTHON = os.path.split(pyPath)[0]
else:
    raise SCons.Errors.UserError('PYTHON must be in PATH')

if (MUC_platform.is_win()):
    tarPath = SCons.Util.WhereIs('tar')
    if tarPath:
        MUC_PATH_TAR = os.path.split(tarPath)[0]
    else:
        paths = [
                r'C:/MSYS/bin',
                r'C:/MSYS/1.0/bin',
                os.path.join( os.environ['ProgramFiles'],  r'MSYS/bin' ),
                os.path.join( os.environ['ProgramFiles'],  r'MSYS/1.0/bin' ),
                'c:/cygwin/bin',
                ]
        MUC_PATH_TAR = None
        for p in paths:
            p = os.path.normpath(p)
            if (os.path.isdir(p)): MUC_PATH_TAR = p
        if (not MUC_PATH_TAR):
            print('Path to access tar not found')


def isIterable(arg):
    """
    Return True if arg is iterable but not a string
    https://stackoverflow.com/questions/1055360/how-to-tell-a-variable-is-iterable-but-not-a-string
    """
    return (
        isinstance(arg, collections.abc.Iterable)
        and not isinstance(arg, six.string_types)
        and not SCons.Util.is_String(arg)
    )

class Environment(SConsEnv):
    """
    Liste statique des scripts ajoutés
    """
    scripts = []
    libpath = []
    libxtrn = []

    def __init__(self, tool, version, *args, **kargs):
        """
        Initialise la partie statique de l'environnement de compilation.
        """
        self.Replace(VERSION = version)
        self.Append(MUC_FORCESTATICLIB = self['forcestatic'])

        try:
            dummy = self['MUC_VERSION_MAIN']
        except KeyError:
            self['MUC_VERSION_MAIN'] = version
        try:
            dummy = self['MUC_VERSION_FULL']
        except KeyError:
            self['MUC_VERSION_FULL'] = self['MUC_VERSION_MAIN']
        self['SPAWN'] = self.spawn

        #---   Ajoute scanreplace pour le fichier .hint.in
        self['INRS_DEV'] = os.environ['INRS_DEV']
        self['INRS_BLD'] = os.environ['INRS_BLD']
        self['INRS_LXT'] = os.environ['INRS_LXT']
        bldDir = self['INRS_BLD']
        toolDir = 'MUC_SCons/SCons/Tool'
        SCons.Environment.apply_tools(self, ['scanreplace'], [os.path.join(bldDir, toolDir)])

        ## #---   Ajoute genHtmlTree pour le fichier .??????
        ## SCons.Environment.apply_tools(self, ['genHtmlTree'], [os.path.join(devDir, toolDir)])

        #---   Ajoute tar (bzip2) comme outil
        SCons.Environment.apply_tools(self, ['packaging', 'tar'], [])
        try:
            dummy = self['WINDOWSDEFSUFFIX']
        except KeyError:
            self['WINDOWSDEFSUFFIX'] = '.def'
        self['TAR']      = 'tar'
        self['TARCOM']   = '$TAR $TARFLAGS --file=${TARGET.posix} --directory=${SOURCES[0]} --exclude=*sconstruct --exclude=*${LIBSUFFIX} --exclude=*${WINDOWSDEFSUFFIX} .'
        self['TARFLAGS'] = self['TARFLAGS'] + '--bzip2'
        self['TARSUFFIX']= '.tar.bz2'

        #---   Conserve à même l'environnement les variables de multi-compilation
        bldBaseDir = self.Dir('#').abspath
        tgtBaseDir = self['targetbasedir']
        self.Append(MUC_BASE     = os.environ['INRS_DEV'])
        self.Append(MUC_CC       = 'python $INRS_BLD/MUC/MultiCompiler.py')
        self.Append(MUC_CXX      = 'python $INRS_BLD/MUC/MultiCompiler.py')
        self.Append(MUC_FTN      = 'python $INRS_BLD/MUC/MultiCompiler.py')
        self.Append(MUC_BUILDPLATEFORM   = self['PLATFORM'])
        self.Replace(MUC_TARGETPLATEFORM = self['MUC_TARGETPLATEFORM']) # S'il n'est pas defini ==> exception
        if (self['fintegersize'] == '4'):
            self.Append(MUC_TARGETPLATEFORMSUFFIX = None)
        elif (self['fintegersize'] == '8'):
            self.Append(MUC_TARGETPLATEFORMSUFFIX = 'i8')
        else:
            raise ValueError('Invalide Fortran INTEGER size: %s' % self['fintegersize'])
        self.Append(MUC_TOOL     = tool)
        self.Append(MUC_TOOL_MAIN= '-'.join( [v for v in (self['MUC_TOOL'], self['MUC_VERSION_MAIN']) if v] ))
        self.Append(MUC_TOOL_FULL= '-'.join( [v for v in (self['MUC_TOOL'], self['MUC_VERSION_FULL']) if v] ))
        self.Append(MUC_BUILDDIR = 'MUC_BUILDDIR: Dummy Placeholder, shall be replaced')
        self.Append(MUC_MPI      = 'MUC_MPI: Dummy Placeholder, shall be replaced')
        self.Append(MUC_BUILD    = 'MUC_BUILD: Dummy Placeholder, shall be replaced')
        self.Append(MUC_LINK     = 'MUC_LINK: Dummy Placeholder, shall be replaced')
        self.Append(MUC_LINK_G   = 'MUC_LINK_G: Dummy Placeholder, shall be replaced')
        self.Append(MUC_TMPINC   = 'MUC_TMPINC: Dummy Placeholder, shall be replaced')
        self.Append(MUC_TMPDFN   = 'MUC_TMPDFN: Dummy Placeholder, shall be replaced')
        self.Append(MUC_TARGET   = 'MUC_TARGET: Dummy Placeholder, shall be replaced')
        self.Append(MUC_TGTERR   = '${MUC_TARGET}.err')
        self.Append(MUC_PCKG     = self['targetbasedir'])
        self.Append(MUC_TGT_ROOT    = os.path.normpath(os.path.join(bldBaseDir, tgtBaseDir)) )
        self.Append(MUC_TGTDIR_BIN  = '_bin')
        self.Append(MUC_TGTDIR_STGE = '_stage')
        self.Append(MUC_TGTDIR_INST = '_install')
        self.Append(MUC_TGTDIR_RUN  = '_run')
        self.Append(MUC_GLBDEFS  = {})

        #---   Change les règles de compilation
        p_ = [
             '--dumpcmd',
             '--compiler=%s'   % '$MUC_TOOL_FULL',
             '--platform=%s'   % '$MUC_TARGETPLATEFORM',
             '--config=%s'     % '$MUC_BUILD',
             '--link=%s'       % '$MUC_LINK',
             '--fortran-integer-size=%s' % '$fintegersize',
             '--include=%s'    % '$MUC_TMPINC',
             '--add-config-from-build',
             '--no-include-from-build',
             '--define=%s'     % '$MUC_TMPDFN',
             '--fortran-modules-directory=%s' % '$FORTRANMODDIR',
             '--output=%s'     % '$TARGET',
             '--error=%s'      % '${TARGET.base}${SOURCES[0].suffix}.err',
             '%s'              % '${SOURCES[0]}'
             ]
        prm = ' '.join(p_)

        for var in ['CC', 'CCCOM', 'SHCCCOM']:
            try:
                dummy = self[var]    # Test si la variable existe
                self[var] = ' '.join( [ self['MUC_CC'], prm ] )
            except KeyError:
                pass
        for var in ['CXX', 'CXXCOM', 'SHCXXCOM']:
            try:
                dummy = self[var]
                self[var] = ' '.join( [ self['MUC_CXX'], prm ] )
            except KeyError:
                pass

        for pre in ['', '_', 'SH', '_SH']:
            for dialect in ['F77', 'F90', 'FORTRAN', 'F95']:
                for var in ['', 'COM', 'PPCOM']:
                    for pst in ['', 'D', 'G']:
                        var_ = '%s%s%s%s' % (pre, dialect, var, pst)
                        try:
                            dummy = self[var_]
                            self[var_] = ' '.join( [ self['MUC_FTN'], prm ] )
                        except KeyError:
                            pass
        modDir = os.path.join(bldBaseDir, self['MUC_PCKG'], Util.addBuildPath('__mod__'))
        self['FORTRANMODDIR'] = os.path.normpath(modDir)

        #---   Modifie l'environnement avec celui du système d'exploitation
        for envKey in os.environ:                 # Ajoute les entrées qui ne sont pas différentes
            if envKey not in self['ENV']:
                self['ENV'].setdefault(envKey, os.environ[envKey])
        #---   Le PATH existant doit être devant celui du système (2019-01-08 pourquoi ???)
        for p in ['PATH', 'LD_LIBRARY_PATH']:
            try:
                path_tmp = self['ENV'][p]
                self['ENV'][p] = os.environ[p]
                self.PrependENVPath(p, path_tmp)
            except:
                pass
        #---   Complète le PATH
        if (MUC_PATH_PYTHON):
            self.PrependENVPath('PATH', MUC_PATH_PYTHON) # PATH de Python
        if (MUC_PATH_TAR):
            self.AppendENVPath('PATH', MUC_PATH_TAR)     # PATH de tar
        self['ENV']['MUC_TOOLS_ROOT'] = self['MUC_BASE'] # Définis MUC_TOOLS_ROOT
        #---   Nettoie le path
        if (MUC_platform.is_win()):
            path_tmp = []
            for p in self['ENV']['PATH'].split(';'):
                path_tmp.append( os.path.normpath(p).strip('"').strip("'") )
            self['ENV']['PATH'] = ';'.join(path_tmp)

        """
        Ajoute les param de la ligne de commande
        """
        self.Replace(**kargs)

        """
        Corrige la longueur de ligne de commande max sous win
        Entre autre, xilink de intel semble avoir besoin de place
        """
        if (MUC_platform.is_win()):
            self['MAXLINELENGTH'] = self['MAXLINELENGTH'] - (256 + 128)
            self['TEMPFILESUFFIX'] = '.txt'
            # Issue #3350
            # Change tempfile argument joining character from a space to a newline
            # mslink will fail if any single line is too long, but is fine with many lines
            # in a tempfile
            # YSe - os.linesep doesn't work and we never had any problem with the space
            self['TEMPFILEARGJOIN'] = ' ' # os.linesep

    """
    Debug print
    """
    def printEnv(self):
        d = self.Dictionary()
        for k in sorted(d):
            print('%s=%s' % (k, d[k]))

    def get_target_arch(self):
        archs = {
                'x86'    : 'ia32',
                'ia32'   : 'ia32',
                'x86-64' : 'x64',
                'x86_64' : 'x64',
                'x64'    : 'x64',
                'amd64'  : 'x64',
                'em64t'  : 'x64',
                'intel64': 'x64',
                'ia64'   : 'ia64',
                }
        data  = {
                ('ia32', 'win32') : 'ilp32',
                ('ia32', 'unx32') : 'ilp32',
                ('x64' , 'win64') : 'llp64',
                ('x64' , 'unx64') : 'lp64',
                ('ia64', 'win64') : 'ilp64',
                ('ia64', 'unx64') : 'ilp64',
                }
        dflt  = {
                'win32' : 'ilp32',
                'unx32' : 'ilp32',
                'win64' : 'llp64',
                'unx64' : 'lp64',
                }

        p = self['MUC_TARGETPLATEFORM']
        try:
            a = self['TARGET_ARCH']
            a = archs[a]
            return data[ (a, p) ]
        except:
            return dflt[p]

    def get_target_pltf(self):
        return self['MUC_TARGETPLATEFORM']

    def get_build_env(self):
        return self['MUC_TOOL']

    def grab_target(self, target = None, source = None, env = None):
        self['MUC_TARGET'] = self.subst('$TARGET', source=source, target=target)
        return 0

    def dup_ucase(self, target = None, source = None, env = None):
        #bdir = self.subst( Util.addBuildPath('__mod__') )   # cf 'FORTRANMODDIR'
        bdir = os.path.join(self['MUC_PCKG'], Util.addBuildPath('__mod__'))
        bdir = self.subst(bdir)
        for s in source:
            for c in s.children():
                b, e = os.path.splitext(c.path)
                p, b = os.path.split(b)
                if p == bdir and e == '.mod':
                    pu = os.path.join(p, b.upper() + e)
                    pl = os.path.join(p, b.lower() + e)
                    if os.path.isfile(pl):
                        shutil.copy2(pl, pu)
                    elif os.path.isfile(pu):
                        shutil.copy2(pu, pl)
        return 0

    def substKeepHOME(self, l):
        toks = ['HOME', 'INRS_DEV', 'INRS_LXT']
        s = l
        for t in toks:
            s = s.replace('$%s/' % t, '_&%s&_/' % t).replace('$%s\\' % t, '_&%s&_\\' % t)
        s = self.subst(s)
        for t in toks:
            s = s.replace('_&%s&_/' % t, '$%s/' % t).replace('_&%s&_\\' % t, '$%s\\' % t)
        if (s[0] == '#'):
            topDir = self.Dir('#').abspath
            s = os.path.join(topDir, s[1:])
        return s

    def substOnlyHOME(self, l):
        toks = ['HOME', 'INRS_DEV', 'INRS_LXT']
        s = l
        for t in toks:
            try:
                r = os.environ[t]
                s = s.replace('$%s/' % t, '%s/' % r).replace('$%s\\' % t, '%s\\' % r)
                s = s.replace('$(%s)' % t, '%s/' % r)
            except:
                pass
        return s

    """
    Set up the dynamic part of the environment
    """
    def setupEnv(self, ctx):
        _ctx = ctx.filterNone()
        self.Replace(SPAWN = self.spawn)
        self.Replace(MUC_BUILD  = _ctx.bld)
        self.Replace(MUC_MPI    = _ctx.mpi)
        self.Replace(MUC_LINK   = _ctx.lnk)
        self.Replace(MUC_LINK_G = _ctx.lnk_g)
        self.Replace(MUC_TMPINC = _ctx.tmpInc)
        self.Replace(MUC_TMPDFN = _ctx.tmpDfn)
        bld_dir = os.path.join('${MUC_TGT_ROOT}', '${MUC_TGTDIR_BIN}', Util.addBuildPath(_ctx.dir))
        bld_dir = os.path.normpath(bld_dir)
        self.Replace(MUC_BUILDDIR = bld_dir)
        self.VariantDir(bld_dir, '.', duplicate = 0)
        #self.VariantDir(bld_dir, '#%s' % _ctx.dir, duplicate = 0)
        #self.Replace(Setup_BuildPath = Util.addBuildPath('').replace('\\', '/'))
        #self.Replace(Setup_SplitBuildPath = Util.addBuildPath('').replace('\\', '/').replace('/', ' '))
        #self.printEnv()
        #raise ctx

        # ---  Includes - temp files
        finc_name = os.path.join(bld_dir, self['MUC_TMPINC'])
        with open(finc_name, 'w') as finc:
            for l in ctx.inc:
                finc.write('-I"%s"\n' % self.substKeepHOME(l))

        # ---  Defines - temp files
        fdef_name = os.path.join(bld_dir, self['MUC_TMPDFN'])
        with open(fdef_name, 'w') as fdef:
            for l, v in self['MUC_GLBDEFS'].items():
                if v: fdef.write('-D"%s"\n' % l)
            for l in ctx.dfn:
                fdef.write('-D"%s"\n' % l)
            if (ctx.lnk == 'static'):
                fdef.write('-U"MODE_DYNAMIC"\n')

        # ---  Libpath
        tmp = [ self.substOnlyHOME(l) for l in ctx.lpt ]
        ctx.lpt = tmp

    def spawn(self, sh, escape, cmd, args, spawnenv):
        cmdenv = {}
        for var in spawnenv:
            cmdenv[var] = spawnenv[var]
        newargs = ' '.join( args[1:] )
        cmdline = ' '.join( (cmd, newargs) )
        # For minor commands, don't overwrite .err file
        # and pass some exceptions
        minorCmd = cmd.split()[0] in ['del', 'rm' ]

        _stdout = None
        _stderr = None
        if not minorCmd:
            tmpNameStdout = self.subst(self['MUC_TGTERR'])
            if tmpNameStdout[:11] != 'MUC_TARGET:':
                _stdout = open(tmpNameStdout, 'w')
                _stderr = subprocess.STDOUT

        # les commandes avec response file ne marche pas avec le shell
        useshell = False
        if '&&' in args:    useshell = True
        if '>'  in cmdline: useshell = True
        if len(args) > 1 and '@' in args[1]: useshell = False
        if MUC_platform.is_unx() and len(cmdline) < self['MAXLINELENGTH']: useshell = True
        # print("===== spawn =")
        # print(cmdline)
        # print('useshell:', useshell)
        # print('stdout:  ', _stdout)
        # print('minorCmd:  ', minorCmd)
        # print("=============")

        rc = None
        try:
            rc = subprocess.call(cmdline,
                                 stdout=_stdout,
                                 stderr=_stderr,
                                 shell = useshell,
                                 env   = cmdenv)
        except FileNotFoundError as e:
            if minorCmd:
                pass
            else:
                print("Unexpected error:", sys.exc_info()[0])
                print("                :", sys.exc_info()[1])
                raise
        except Exception as e:
            print("Unexpected error:", sys.exc_info()[0])
            print("                :", sys.exc_info()[1])
            raise
        if (_stdout): _stdout.close()

        if rc and _stdout:
            print("===== spawn error: stdout ======")
            try:
                tmpFileStdout = open(tmpNameStdout, 'r')
                print(tmpNameStdout)
                print(tmpFileStdout.readlines())
                tmpFileStdout.close()
                print('command line:')
                print(cmdline)
                os.unlink(tmpNameStdout)
            except:
                pass
            print("================================")
        return rc


    """
    Rule to add explicit dependencies
    to all the children
    """
    def MUC_MakeChildDepends(self, tgt, dep):
        for t in tgt:
            self.Depends( t.all_children(), dep)
        return None

    """
    Rule to run a command
    """
    def MUC_Command(self, ctx, act, is_abspath = False):
        def build_it(target = None, source = None, env = None):
            act = env.subst('${MUC_ACTION}', source=source, target=target)
            if act[0] == '#': act = act[1:]
            print('MUC_Command.build_it: ', act)
            env.Execute(act)
            return 0

        localEnv = self.Clone()
        ExternalLib.setupLxt(ctx, localEnv)
        localEnv.setupEnv(ctx)

        if (localEnv['MUC_TARGETPLATEFORM'] in ['unx32', 'unx64']):
            for p in ctx.lpt:
                localEnv.PrependENVPath('LD_LIBRARY_PATH', p)

        ctx.src = Util.gristSrc(ctx)
        tgt = os.path.join(localEnv['MUC_BUILDDIR'], ctx.tgt)
        if (is_abspath):
            localEnv.Replace(MUC_ACTION = act)
        else:
            pth = os.path.join(localEnv['MUC_BUILDDIR'], act)
            localEnv.Replace(MUC_ACTION = pth)
        return localEnv.Command(tgt, ctx.src, build_it)

    """
    Rule to build an object
    """
    def MUC_Object(self, ctx):
        ctx.inc = Util.gristIncPath(ctx)
        ctx.src = Util.gristSrc(ctx)

        localEnv = self.Clone()
        localEnv.setupEnv(ctx)
        r0 = localEnv.Object(ctx.tgt,
                             ctx.src,
                             CPATH = ctx.inc,
                             CPPPATH = ctx.inc,
                             FORTRANPATH = ctx.inc + [ localEnv['FORTRANMODDIR'] ] )
        r2 = localEnv.AddPostAction(r0, localEnv.dup_ucase)
        return r0

    """
    Rule to build a static library
    """
    def MUC_StaticLibrary(self, ctx):
        ctx.inc = Util.gristIncPath(ctx)
        ctx.src = Util.gristSrc(ctx)
        # win does not have lib of dynamic objects
        #if self.get_target_pltf() in ['win32', 'win64']:
        #    ctx.lnk = 'static'

        localEnv = self.Clone()
        ExternalLib.setupLxt(ctx, localEnv)
        localEnv.setupEnv(ctx)

        pth = os.path.join(localEnv['MUC_BUILDDIR'], ctx.tgt)
        src = []
        for rc in ctx.src:
            if (os.path.splitext(rc)[1] == '.rc'):
                pass
            else:
                src.append(rc)
        r0 = localEnv.StaticLibrary(pth,
                                    src,
                                    CPATH   = ctx.inc,
                                    CPPPATH = ctx.inc,
                                    FORTRANPATH   = ctx.inc + [ localEnv['FORTRANMODDIR'] ],
                                    F90PATH       = ctx.inc + [ localEnv['FORTRANMODDIR'] ],
                                    FORTRANMODDIR = localEnv['FORTRANMODDIR'],
                                    F90MODDIR     = localEnv['FORTRANMODDIR'])
        r1 = localEnv.AddPreAction (r0, localEnv.grab_target)
        r2 = localEnv.AddPostAction(r0, localEnv.dup_ucase)
        return r0

    """
    Rule to build a shared library
    """
    def MUC_SharedLibrary(self, ctx):
        if (self['MUC_FORCESTATICLIB']):
            Environment.scripts = [ctx.tgt] + ctx.lib + Environment.scripts
            Environment.libpath.extend(ctx.lpt)
            Environment.libxtrn.extend(ctx.lxt)
            return self.MUC_StaticLibrary(ctx)

        if ctx.lnk != 'dynamic':
            raise SCons.Errors.UserError('Static link requested for a shared library')
        ctx.inc = Util.gristIncPath(ctx)
        ctx.lib = Util.gristLib    (ctx)
        ctx.lpt = Util.gristLibPath(ctx)
        ctx.src = Util.gristSrc    (ctx)

        localEnv = self.Clone()

        ExternalLib.setupLxt(ctx, localEnv)
        localEnv.setupEnv(ctx)
        ctx.lib.extend(ctx.sys)

        pth = os.path.join(localEnv['MUC_BUILDDIR'], ctx.tgt)
        src = []
        for rc in ctx.src:
            if (os.path.splitext(rc)[1] == '.rc'):
                try:
                    src.append(localEnv.RES(rc))
                except:
                    pass
            else:
                src.append(rc)
        r0 = localEnv.SharedLibrary(pth,
                                    src,
                                    LIBPATH = ctx.lpt,
                                    LIBS    = ctx.lib,
                                    CPATH   = ctx.inc,
                                    CPPPATH = ctx.inc,
                                    FORTRANPATH   = ctx.inc + [ localEnv['FORTRANMODDIR'] ],
                                    F90PATH       = ctx.inc + [ localEnv['FORTRANMODDIR'] ],
                                    FORTRANMODDIR = localEnv['FORTRANMODDIR'],
                                    F90MODDIR     = localEnv['FORTRANMODDIR'])
        r1 = localEnv.AddPreAction (r0, localEnv.grab_target)
        r2 = localEnv.AddPostAction(r0, localEnv.dup_ucase)
        return r0

    """
    Rule to build a program
    """
    def MUC_Program(self, ctx):
        if self['MUC_FORCESTATICLIB']:
            Environment.scripts = ctx.lib + Environment.scripts
            Environment.libpath.extend(ctx.lpt)
            Environment.libxtrn.extend(ctx.lxt)
            ctx.lib = Util.seq_uniquer_on_last(Environment.scripts)
            ctx.lpt = Util.seq_uniquer_on_last(Environment.libpath)
            ctx.lxt = Util.seq_uniquer_on_last(Environment.libxtrn)

        ctx.inc = Util.gristIncPath(ctx)
        ctx.lib = Util.gristLib    (ctx)
        ctx.lpt = Util.gristLibPath(ctx)
        ctx.src = Util.gristSrc    (ctx)

        localEnv = self.Clone()

        ExternalLib.setupLxt(ctx, localEnv)
        localEnv.setupEnv(ctx)
        ctx.lib.extend(ctx.sys)

        pth = os.path.join('${MUC_BUILDDIR}', ctx.tgt)
        src = []
        for rc in ctx.src:
            if (os.path.splitext(rc)[1] == '.rc'):
                try:
                    src.append(localEnv.RES(rc))
                except:
                    pass
            else:
                src.append(rc)
        r0 = localEnv.Program(pth,
                              src,
                              LIBPATH = ctx.lpt,
                              LIBS    = ctx.lib,
                              CPATH   = ctx.inc,
                              CPPPATH = ctx.inc,
                              FORTRANPATH = ctx.inc + [ localEnv['FORTRANMODDIR'] ],
                              F90PATH     = ctx.inc + [ localEnv['FORTRANMODDIR'] ])
        r1 = localEnv.AddPreAction (r0, localEnv.grab_target)
        r2 = localEnv.AddPostAction(r0, localEnv.dup_ucase)
        return r0

    """
    Rule to copy files to the stage
    """
    def MUC_Stage(self, ctx, f, subdir = '', stripDir = None):
        if not self['dostage']: return []
        if stripDir is None: stripDir = (subdir != '')

        res_ = []
        try:
            localEnv = self.Clone()
            localEnv.setupEnv(ctx)

            dir_ = Util.gristStage (ctx)
            dir_ = os.path.join(dir_, subdir)
            for src_ in f:
                dstL_ = src_ if isIterable(src_) else [src_]
                for dst_ in dstL_:
                    dst_ = Util.ungristFile(str(dst_), ctx)
                    if stripDir: dst_ = os.path.split(dst_)[1]
                    dst_ = os.path.join(dir_, os.path.dirname(dst_))
                    res_ += localEnv.Install(dst_, src_)
        except Exception as e:
            import traceback
            errMsg = 'Warning: Passing exception\n%s\n%s' % (str(e), traceback.format_exc())
            print(errMsg)
            pass
        return res_

    """
    Rule to tar the stage
    """
    def MUC_TarBz2(self, ctx, f = None):
        #try:
        if (True):
            if (self['doinstall']):
                localEnv = self.Clone()
                localEnv.setupEnv(ctx)
                if (MUC_PATH_TAR):
                    localEnv.PrependENVPath('PATH', MUC_PATH_TAR)     # PATH de tar

                s = Util.gristStage(ctx)
                t = os.path.join(Util.gristInstall(ctx), ctx.prj)
                t = os.path.join(t, ctx.prj + '.tar.bz2')
                b = localEnv.Tar(t, [s, f] )
                #b = localEnv.Package(t,
                #                     f,
                #                     PACKAGEROOT = s,
                #                     NAME = t,
                #                     VERSION        = '1.2.3',
                #                     PACKAGETYPE    = 'tarbz2',
                #                     LICENSE        = 'lgpl',
                #                    )

                t = os.path.join(Util.gristInstall(ctx), ctx.prj, 'setup.hint')
                return localEnv.ScanReplace(t, [r'setup.hint.in'])
        #except:
        #    return None

    """
    Rule to copy files to the run directory

    stripDir shall be specified as a boolean value (True or False).
    None is special value to mark for a default (absent) value.
    """
    def MUC_Run_One(self, dst, src):
        return self.Install(dst, src)

    def MUC_Run(self, ctx, files, subdir = '', stripDir = None):
        if stripDir is None: stripDir = (subdir != '')

        res_ = []
        try:
            localEnv = self.Clone()
            localEnv.setupEnv(ctx)

            dir_ = Util.gristRun(ctx)
            dir_ = os.path.join(dir_, subdir)
            for src_ in files:
                #dstL_ = [src_] if SCons.Util.is_String(src_) else src_
                dstL_ = src_ if isIterable(src_) else [src_]
                for dst_ in dstL_:
                    dst_ = Util.ungristFile(str(dst_), ctx)
                    if stripDir: dst_ = os.path.split(dst_)[1]
                    dst_ = os.path.join(dir_, os.path.dirname(dst_))
                    res_ += localEnv.MUC_Run_One(dst_, src_)
        except Exception as e:
            import traceback
            errMsg = 'Warning: Passing exception\n%s\n%s' % (str(e), traceback.format_exc())
            print(errMsg)
            pass
        #raise
        return res_
