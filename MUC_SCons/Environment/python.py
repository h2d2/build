# -*- coding: UTF-8 -*-
import SCons.Script

from . import Environment
import MUC_platform
import Util

import os

SConsEnv = SCons.Environment.Environment
MUCEnv   = Environment.Environment

MUC_PYTHON_TKN = 'dummy_target_for_python_construction'

class EnvironmentPython(MUCEnv):
    def __init__(self, tool, version, opts, *args, **kwargs):
        if (MUC_platform.is_win()):
            muc_target_plateform = 'win64'
        elif (MUC_platform.is_unx()):
            muc_target_plateform = 'unx64'
        else:
            raise SCons.Errors.UserError('Invalid python platform: %s' % MUC_platform.platform())

        muc_build_plateform = MUC_platform.build_platform()
        tools =  ['python']

        kwargs.setdefault('MUC_TARGETPLATEFORM', muc_target_plateform)
        SConsEnv.__init__( self,
                           tools = tools,
                           options = opts,
                           plateform = muc_build_plateform,
                           MUC_TARGETPLATEFORM = muc_target_plateform)
        MUCEnv.__init__(self, tool, version, *args, **kwargs)

    def setupEnv(self, ctx):
        MUCEnv.setupEnv(self, ctx)

    @staticmethod
    def getTokenFile(prj):
        return '%s_of___%s___.txt' % (MUC_PYTHON_TKN, prj)

    def getAllTokenFiles(self, prj):
        return [ EnvironmentPython.getTokenFile(prj) ]

    """
    Rule to build an python file
    """
    def MUC_Python(self, ctx, spt):
        """
        Use a dummy NoOp builder to pack all the files as one target.
        If this is not done, dependencies are not detected globally.
        MUC_Python will consume and remove suitable source files from
        spt (script list).
        """
        def build_function(target, source, env):
            env.Execute("echo . > %s" % env.subst(target)[0])
            return None     # None for success

        NoOpBuilder = SCons.Builder.Builder(action = build_function)

        localEnv = self.Clone()
        localEnv.setupEnv(ctx)
        localEnv.Append(BUILDERS = {'NoOpPython' : NoOpBuilder})
        localEnv.Decider('make')        # token file has no content, just timestamp

        r0 = []
        toBeRemoved = []
        for src in spt:
            if os.path.splitext(src)[1] == '.py':
                tgt = src
                tgt = os.path.join('${MUC_BUILDDIR}', tgt)  # prepend build dir
                tgt = os.path.dirname(tgt)
                r0_ = localEnv.Install(tgt, src)
                r0.append(r0_)
                toBeRemoved.append(src)
        tkn = EnvironmentPython.getTokenFile(ctx.prj)       # token file for dependency
        tgt = os.path.join('${MUC_BUILDDIR}', tkn)
        _ = localEnv.NoOpPython(tgt, r0)

        # ---  Remove items from script list
        for item in toBeRemoved:
            try:
                spt.remove(item)
            except ValueError:
                pass
        return r0

    def MUC_Cython(self, ctx):
        return []

