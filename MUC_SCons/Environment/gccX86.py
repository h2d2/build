# -*- coding: UTF-8 -*-

from . import gcc
import MUC_platform


MUCGcc = gcc.EnvironmentGcc

class EnvironmentGccX86(MUCGcc):
    def __init__(self, tool, version, opts, *args, **kwargs):
        if (MUC_platform.is_win()):
            muc_target_plateform = 'win32'
        elif (MUC_platform.is_unx()):
            muc_target_plateform = 'unx32'
        else:
            raise SCons.Errors.UserError('Invalid gcc platform: %s' % MUC_platform.platform())

        kwargs.setdefault('MUC_TARGETPLATEFORM', muc_target_plateform)
        MUCGcc.__init__(self, tool, version, opts, *args, **kwargs)
