# -*- coding: UTF-8 -*-
import ExternalLib

import Util
import os

class google_breakpad(ExternalLib.ExternalLib):
    def __go_unx(self, ctx):
        bdir = os.environ['INRS_LXT']
        bdir = os.path.join(bdir, 'breakpad', '_run')
        hdir = os.path.join(bdir, 'include')
        ldir = os.path.join(bdir, 'lib')
        __inc = [ hdir ]
        __lpt = [ ldir ]
        __lib = [ 'breakpad_client' ]
        self.extendCtx(ctx, __lpt, __lib, __inc)

        hdir = os.environ['INRS_LXT']
        hdir = os.path.join(hdir, 'h2d2-breakpad')
        rdir = os.path.join(hdir, '_run', Util.addBuildPath(r'External'))
        __inc = [ os.path.join(hdir, 'source') ]
        __lpt = [ os.path.join(rdir, 'bin') ]
        __lib = [ 'getDumpPath' ]
        self.extendCtx(ctx, __lpt, __lib, __inc)


    def __go_win(self, ctx, sdir):
        hdir = os.environ['INRS_LXT']
        hdir = os.path.join(hdir, 'breakpad', 'src')
        ldir = os.path.join(hdir, 'client', 'windows', ctx.bld, 'lib', sdir, ctx.lnk)
        __inc = [ hdir ]
        __lpt = [ ldir ]
        __lib = [ 'exception_handler', 'crash_generation_server', 'crash_generation_client', 'crash_report_sender', 'common' ]
        self.extendCtx(ctx, __lpt, __lib, __inc)

        hdir = os.environ['INRS_LXT']
        hdir = os.path.join(hdir, 'h2d2-breakpad')
        rdir = os.path.join(hdir, '_run', Util.addBuildPath(r'External'))
        __inc = [ os.path.join(hdir, 'source') ]
        __lpt = [ os.path.join(rdir, 'bin') ]
        __lib = [ 'getDumpPath' ]
        self.extendCtx(ctx, __lpt, __lib, __inc)

    def __cmc_unx32(self, ctx):
        self.__go_unx(ctx)


    def __gcc_unx32(self, ctx):
        self.__go_unx(ctx)

    def __gccX86_unx32(self, ctx):
        self.__go_unx(ctx)

    def __intel_unx32(self, ctx):
        self.__go_unx(ctx)

    def __itlX86_unx32(self, ctx):
        self.__go_unx(ctx)

    def __sun_unx32(self, ctx):
        self.__go_unx(ctx)

    def __gcc_unx64(self, ctx):
        self.__go_unx(ctx)

    def __gccX64_unx64(self, ctx):
        self.__go_unx(ctx)

    def __intel_unx64(self, ctx):
        self.__go_unx(ctx)

    def __itlX64_unx64(self, ctx):
        self.__go_unx(ctx)

    def __itlI64_unx64(self, ctx):
        self.__go_unx(ctx)

    def __open64_unx64(self, ctx):
        self.__go_unx(ctx)

    def __sun_unx64(self, ctx):
        self.__go_unx(ctx)

    def __gcc_unx64i8(self, ctx):
        self.__go_unx(ctx)

    def __gccX64_unx64i8(self, ctx):
        self.__go_unx(ctx)

    def __intel_unx64i8(self, ctx):
        self.__go_unx(ctx)

    def __itlX64_unx64i8(self, ctx):
        self.__go_unx(ctx)

    def __itlI64_unx64i8(self, ctx):
        self.__go_unx(ctx)

    def __open64_unx64i8(self, ctx):
        self.__go_unx(ctx)

    def __sun_unx64i8(self, ctx):
        self.__go_unx(ctx)


    def __gcc_win32(self, ctx):
        self.__go_win(ctx, 'win32')

    def __gccX86_win32(self, ctx):
        self.__go_win(ctx, 'win32')

    def __msvc_win32(self, ctx):
        self.__go_win(ctx, 'win32')

    def __intel_win32(self, ctx):
        self.__go_win(ctx, 'win32')

    def __itlX86_win32(self, ctx):
        self.__go_win(ctx, 'win32')

    def __gcc_win64(self, ctx):
        self.__go_win(ctx, 'win64')

    def __gccX64_win64(self, ctx):
        self.__go_win(ctx, 'win64')

    def __intel_win64(self, ctx):
        self.__go_win(ctx, 'win64')

    def __itlX64_win64(self, ctx):
        self.__go_win(ctx, 'win64')

    def __itlI64_win64(self, ctx):
        self.__go_win(ctx, 'win64')

    def __intel_win64i8(self, ctx):
        self.__go_win(ctx, 'win64')

    def __itlX64_win64i8(self, ctx):
        self.__go_win(ctx, 'win64')

    def __itlI64_win64i8(self, ctx):
        self.__go_win(ctx, 'win64')

    def __ftnchek_win32(self, ctx):
        pass

    def configure(self, ctx, tool, version, plateform):
        fnm = '_google_breakpad__%s_%s' % (tool, plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)

