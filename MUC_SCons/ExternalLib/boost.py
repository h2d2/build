# -*- coding: UTF-8 -*-
import ExternalLib

import Util
import os.path

class boost(ExternalLib.ExternalLib):
    def __go(self, ctx, ptf):
        hdir = Util.getPathFromVersion(os.environ['INRS_LXT'], r'boost', '', '_', '_')
        ldir = os.path.join(hdir, r'_run', ptf, r'release', r'lib')
        __inc = [ hdir ]
        __lpt = [ ldir ]
        __lib = [ 'libboost_thread', 'libboost_filesystem', 'libboost_system' ]
        self.extendCtx(ctx, __lpt, __lib, __inc)

    def __unx32(self, ctx):
        self.__go(ctx, 'unx32')

    def __unx64(self, ctx):
        self.__go(ctx, 'unx64')

    def __unx64i8(self, ctx):
        self.__go(ctx, 'unx64')

    def __win32(self, ctx):
        self.__go(ctx, 'win32')

    def __win64(self, ctx):
        self.__go(ctx, 'win64')

    def __win64i8(self, ctx):
        self.__go(ctx, 'win64')

    def configure(self, ctx, tool, version, plateform):
        fnm = '_boost__%s' % (plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)
