# -*- coding: UTF-8 -*-
import ExternalLib

import Util
import MUC_platform
import SCons
import os.path

# https://stackoverflow.com/questions/43865291/import-function-from-a-file-in-the-same-folder?rq=1
from ExternalLib import intel_path

class mkl(ExternalLib.ExternalLib):
    # https://software.intel.com/en-us/articles/intel-mkl-link-line-advisor

    #---------------
    #---    Linux 32
    #---------------
    def __intel_unx32(self, ctx):
        self.__itlX86_unx32(ctx)

    def __itlX86_unx32(self, ctx):
        __lpt = [ intel_path.get_intel_path('unx32', self.vers) ]
        __lib = [ 'mkl_scalapack_core', 'mkl_blacs' ]
        self.extendCtx(ctx, __lpt, __lib)

    #---------------
    #---    Linux 64
    #---------------
    def __intel_unx64(self, ctx):
        self.__itlX64_unx64(ctx)

    def __itlX64_unx64(self, ctx):
        __lpt = [ intel_path.get_intel_path('unx64', self.vers) ]
        if   ctx.mpi[0:7] == 'openmpi':
            __lib = [ 'mkl_scalapack_lp64', 'mkl_blacs_openmpi_lp64' ]
        elif ctx.mpi[0:8] == 'intelmpi':
            __lib = [ 'mkl_scalapack_lp64', 'mkl_blacs_intelmpi_lp64' ]
        else:
            raise SCons.Errors.UserError('Invalid MPI system: %s\nExpected value in [openmpi, intelmpi]' % ctx.mpi)
        self.extendCtx(ctx, __lpt, __lib)

    #---------------
    #---    Linux 64i8
    #---------------
    def __intel_unx64i8(self, ctx):
        self.__itlX64_unx64i8(ctx)

    def __itlX64_unx64i8(self, ctx):
        __lpt = [ intel_path.get_intel_path('unx64', self.vers) ]
        if   ctx.mpi[0:7] == 'openmpi':
            __lib = [ 'mkl_scalapack_ilp64', 'mkl_blacs_openmpi_ilp64' ]
        elif ctx.mpi[0:8] == 'intelmpi':
            __lib = [ 'mkl_scalapack_ilp64', 'mkl_blacs_intelmpi_ilp64' ]
        else:
            raise SCons.Errors.UserError('Invalid MPI system: %s\nExpected value in [openmpi, intelmpi]' % ctx.mpi)
        self.extendCtx(ctx, __lpt, __lib)

    #---------------
    #---    CMC
    #---------------
    def __cmcitl(self, ctx):
        __lpt = []  # Path are set by s.f90
        if   ctx.mpi[0:7] == 'openmpi':
            __lib = [ 'mkl_scalapack_lp64', 'mkl_blacs_openmpi_lp64' ]
        elif ctx.mpi[0:8] == 'intelmpi':
            __lib = [ 'mkl_scalapack_lp64', 'mkl_blacs_intelmpi_lp64' ]
        else:
            raise SCons.Errors.UserError('Invalid MPI system: %s\nExpected value in [openmpi, intelmpi]' % ctx.mpi)
        self.extendCtx(ctx, __lpt, __lib)

    def __cmc_unx64(self, ctx):
        self.__cmcitl(ctx)

    def __cmcitl_unx64(self, ctx):
        self.__cmcitl(ctx)

    #-----------------
    #---    Windows 32
    #-----------------
    def __msvc_win32(self, ctx):
        __lpt = [ intel_path.get_intel_path('win32', self.vers) ]
        __lib = ['mkl_intel_c', 'mkl_intel_thread', 'mkl_core']
        self.extendCtx(ctx, __lpt, __lib)

    def __intel_win32(self, ctx):
        self.__itlX86_win64(ctx)

    def __itlX86_win32(self, ctx):
        __lpt = [ intel_path.get_intel_path('win32', self.vers) ]
        __lib = [ 'mkl_scalapack_core_dll', 'mkl_blacs_dll' ]
        self.extendCtx(ctx, __lpt, __lib)

    #-----------------
    #---    Windows 64
    #-----------------
    def __intel_win64(self, ctx):
        self.__itlX64_win64(ctx)

    def __itlX64_win64(self, ctx):
        __lpt = [ intel_path.get_intel_path('win64', self.vers) ]
        __lib = [ 'mkl_scalapack_lp64_dll', 'mkl_blacs_lp64_dll' ]
        self.extendCtx(ctx, __lpt, __lib)

    #-----------------
    #---    Windows 64 i8
    #-----------------
    def __intel_win64i8(self, ctx):
        self.__itlX64_win64i8(ctx)

    def __itlX64_win64i8(self, ctx):
        __lpt = [ intel_path.get_intel_path('win64', self.vers) ]
        if ctx.mpi[0:8] == 'intelmpi':
            __lib = [ 'mkl_scalapack_ilp64_dll', 'mkl_blacs_ilp64_dll' ]
        else:
            raise SCons.Errors.UserError('Invalid MPI system: %s\nExpected value in [intelmpi]' % ctx.mpi)
        self.extendCtx(ctx, __lpt, __lib)

    def configure(self, ctx, tool, version, plateform):
        self.tool = tool
        self.vers = version
        self.pltf = plateform
        fnm = '_mkl__%s_%s' % (tool, plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)

class sun(ExternalLib.ExternalLib):
    def __get_sun_path_unx32(self):
        vmaj = '.'.join( self.vers.split('.')[0:2])
        try:
            base = {
                   '12.1' : 'sunstudio',
                   '12.2' : 'solstudio',
                   '12.3' : 'solarisstudio',
                   '12.4' : 'solarisstudio',
                   '12.5' : 'developerstudio',
                   '12.6' : 'developerstudio',
                   }
            name = base[vmaj] + vmaj
        except:
            raise SCons.Errors.UserError('blas: Invalid sun version: %s' % self.vers)
        p = os.path.join(r'/opt/sun', name, 'lib')
        return os.path.normpath(p)

    def __get_sun_path_unx64(self):
        p = os.path.join(self.__get_sun_path_unx32(), 'amd64')
        return os.path.normpath(p)

    #---------------
    #---    Linux 64
    #---------------
    def __sun_unx64(self, ctx):
        vmaj = float( '.'.join( self.vers.split('.')[0:2]) )
        if (vmaj < 12.3):
            __lpt = [ self.__get_sun_path_unx64() ]
            __lib = [ 'scalapack', 'blacs_openmpi' ]
            self.extendCtx(ctx, __lpt, __lib)
        else:
            netlib().configure(ctx, self.tool, self.vers, self.pltf)

    #---------------
    #---    Linux 64i8
    #---------------
    def __sun_unx64i8(self, ctx):
        self.__sun_unx64(ctx)

    #---------------
    #---    Linux 32
    #---------------
    def __sun_unx32(self, ctx):
        vmaj = float( '.'.join( self.vers.split('.')[0:2]) )
        if (vmaj < 12.3):
            __lpt = [ self.__get_sun_path_unx32() ]
            __lib = [ 'scalapack', 'blacs_openmpi' ]
            self.extendCtx(ctx, __lpt, __lib)
        else:
            netlib().configure(ctx, self.tool, self.vers, self.pltf)

    def configure(self, ctx, tool, version, plateform):
        self.tool = tool
        self.vers = version
        self.pltf = plateform
        fnm = '_sun__%s_%s' % (tool, plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)

class netlib(ExternalLib.ExternalLib):
    def __netlib_conf(self, ctx):
        path = None
        try:
            root = Util.getPathFromVersion( os.environ['INRS_LXT'], 'scalapack' )
            isVersion2 = root.split('scalapack-')[1].split('.')[0] >= '2'
            mpi = '-'.join( (ctx.mpi, ctx.env['MUC_TARGETPLATEFORM']))
            if ctx.env['MUC_TARGETPLATEFORMSUFFIX'] == 'i8':
                mpi += ctx.env['MUC_TARGETPLATEFORMSUFFIX']
            path = os.path.join(root, 'lib', mpi, ctx.lnk)
            if (not os.path.isdir(path)):
                path = os.path.join(root, 'lib')
            __lpt = [ path ]
            if (isVersion2):
                __lib = [ 'scalapack' ]
            else:
                __lib = [ 'scalapack', 'blacs', 'blacsC', 'blacsF77', 'blacs' ]
        except:
            path = Util.getPathOfFile('/usr/lib', 'libblacs-' + ctx.mpi + '.a')
            path = os.path.dirname(path)
            __lpt = [ path, gcc_path ]
            __lib = [ 'scalapack', 'blacs', 'blacsCinit', 'blacsF77init', 'blacs' ]
            __lib = ['-'.join([x,ctx.mpi]) for x in __lib]

        self.extendCtx(ctx, __lpt, __lib)

    def __gcc_conf(self, ctx):
        gcc_path = Util.getAllPathOfFile('/usr/lib/gcc', 'libgfortran.a')
        gcc_path = [ os.path.dirname(d) for d in gcc_path ]
        gcc_path = Util.getPathFromVersion(gcc_path, '', search_join='')

        __lpt = [ gcc_path ]
        __lib = [ 'gfortran' ]

        self.extendCtx(ctx, __lpt, __lib)

    def configure(self, ctx, tool, version, plateform):
        self.__netlib_conf(ctx)
        if (plateform in ['unx32', 'unx64', 'unx64i8']):
            self.__gcc_conf(ctx)

class blacs(ExternalLib.ExternalLib):
    #---------------
    #---    CMC
    #---------------
    def __cmc_unx32(self, ctx):
        raise SCons.Errors.UserError("Invalide platform combination: %s" % "cmc_unx32")

    def __cmcitl_unx32(self, ctx):
        raise SCons.Errors.UserError("Invalide platform combination: %s" % "cmc_unx32")

    def __cmcpgi_unx32(self, ctx):
        raise SCons.Errors.UserError("Invalide platform combination: %s" % "cmc_unx32")

    def __cmc_unx64(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __cmcitl_unx64(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __cmcpgi_unx64(self, ctx):
        raise SCons.Errors.UserError("Invalide platform combination: %s" % "cmc_unx32")

    #---------------
    #---    Linux 64
    #---------------
    def __gcc_unx64(self, ctx):
        netlib().configure(ctx, self.tool, self.vers, self.pltf)

    def __gccX64_unx64(self, ctx):
        netlib().configure(ctx, self.tool, self.vers, self.pltf)

    def __intel_unx64(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __itlX64_unx64(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __itlX64_unx64i8(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __open64_unx64(self, ctx):
        netlib().configure(ctx, self.tool, self.vers, self.pltf)

    def __sun_unx64(self, ctx):
        sun().configure(ctx, self.tool, self.vers, self.pltf)

    #---------------
    #---    Linux 64i8
    #---------------
    def __gcc_unx64i8(self, ctx):
        netlib().configure(ctx, self.tool, self.vers, self.pltf)

    def __gccX64_unx64i8(self, ctx):
        netlib().configure(ctx, self.tool, self.vers, self.pltf)

    def __intel_unx64i8(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __itlX64_unx64i8(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __open64_unx64i8(self, ctx):
        netlib().configure(ctx, self.tool, self.vers, self.pltf)

    def __sun_unx64i8(self, ctx):
        sun().configure(ctx, self.tool, self.vers, self.pltf)

    #---------------
    #---    Linux 32
    #---------------
    def __gcc_unx32(self, ctx):
        netlib().configure(ctx, self.tool, self.vers, self.pltf)

    def __gccX86_unx32(self, ctx):
        netlib().configure(ctx, self.tool, self.vers, self.pltf)

    def __intel_unx32(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __itlX86_unx32(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __sun_unx32(self, ctx):
        sun().configure(ctx, self.tool, self.vers, self.pltf)

    #-----------------
    #---    Windows 32
    #-----------------
    def __gcc_win32(self, ctx):
        netlib().configure(ctx, self.tool, self.vers, self.pltf)

    def __gccX86_win32(self, ctx):
        netlib().configure(ctx, self.tool, self.vers, self.pltf)

    def __msvc_win32(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __intel_win32(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __itlX86_win32(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    #-----------------
    #---    Windows 64
    #-----------------
    def __gcc_win64(self, ctx):
        netlib().configure(ctx, self.tool, self.vers, self.pltf)

    def __gccX64_win64(self, ctx):
        netlib().configure(ctx, self.tool, self.vers, self.pltf)

    def __intel_win64(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __intel_win64i8(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __itlX64_win64(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __itlX64_win64i8(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __itlI64_win64(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __itlI64_win64i8(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)


    def __ftnchek_win32(self, ctx):
        pass

    def configure(self, ctx, tool, version, plateform):
        self.tool = tool
        self.vers = version
        self.pltf = plateform
        fnm = '_blacs__%s_%s' % (tool, plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)


