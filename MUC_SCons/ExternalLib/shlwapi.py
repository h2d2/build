# -*- coding: UTF-8 -*-
import ExternalLib

class shlwapi(ExternalLib.ExternalLib):
    def __unx(self, ctx):
        pass

    def __win(self, ctx):
        __lpt = [ ]
        __lib = ['shlwapi']
        self.extendCtx(ctx, __lpt, __lib)


    def __unx32(self, ctx):
        self.__unx(ctx)

    def __unx64(self, ctx):
        self.__unx(ctx)

    def __unx64i8(self, ctx):
        self.__unx(ctx)

    def __win32(self, ctx):
        self.__win(ctx)

    def __win64(self, ctx):
        self.__win(ctx)

    def __win64i8(self, ctx):
        self.__win(ctx)

    def configure(self, ctx, tool, version, plateform):
        fnm = '_shlwapi__%s' % (plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)
