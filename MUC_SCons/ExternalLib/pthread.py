# -*- coding: UTF-8 -*-
import ExternalLib

import Util
import os

class pthread(ExternalLib.ExternalLib):
    def __unx(self, ctx):
        __lpt = [ ]
        __lib = ['pthread']
        self.extendCtx(ctx, __lpt, __lib)

    def __unx32(self, ctx):
       self.__unx(ctx)

    def __unx64(self, ctx):
       self.__unx(ctx)

    def __unx64i8(self, ctx):
       self.__unx(ctx)

    def __cmc(self, ctx):
        if 'mpi' not in ctx.lxt:
            self.__unx(ctx)

    def __win(self, ctx):
        hdir = os.environ['INRS_LXT']
        hdir = Util.getPathFromVersion(hdir, 'pthreads4w', search_join='-v')
        __inc = [ os.path.join(hdir, 'include') ]
        __lpt = [ os.path.join(hdir, 'lib') ]
        __lib = [ 'pthreadVC3' ]
        self.extendCtx(ctx, __lpt, __lib, __inc)

    def __win32(self, ctx):
       self.__win(ctx)

    def __win64(self, ctx):
       self.__win(ctx)

    def __win64i8(self, ctx):
       self.__win(ctx)

    def configure(self, ctx, tool, version, plateform):
        if tool in ['cmc', 'cmcitl', 'cmcpgi']:
            fnm = '_pthread__cmc'
        else:
            fnm = '_pthread__%s' % (plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)
