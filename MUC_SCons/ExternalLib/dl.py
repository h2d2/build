# -*- coding: UTF-8 -*-
import ExternalLib
import Util
import os

class dl(ExternalLib.ExternalLib):
    def __cnf_sta_unx(self, ctx):
        hdir = os.path.join(os.environ['INRS_LXT'], 'FakeDLL/_run', Util.addBuildPath(r'External'))
        hdir = os.path.normpath(hdir)
        __inc = [ hdir ]
        __lpt = [ hdir ]
        __lib = ['fakedll', 'dl']
        __dfn = ['FAKE_DLL']
        self.extendCtx(ctx, __lpt, __lib, __inc, __dfn)

    def __cnf_sta_win(self, ctx):
        hdir = os.path.join(os.environ['INRS_LXT'], 'FakeDLL/_run', Util.addBuildPath(r'External'))
        hdir = os.path.normpath(hdir)
        __inc = [ hdir ]
        __lpt = [ hdir ]
        __lib = ['fakedll']
        __dfn = ['FAKE_DLL']
        self.extendCtx(ctx, __lpt, __lib, __inc, __dfn)

    def __cnf_dyn_unx(self, ctx):
        hdir = os.path.join(os.environ['INRS_LXT'], 'FakeDLL/_run', Util.addBuildPath(r'External'))
        hdir = os.path.normpath(hdir)
        __inc = [ hdir ]
        __lpt = [ ]
        __lib = ['dl']
        self.extendCtx(ctx, __lpt, __lib, __inc)

    def __cnf_dyn_win(self, ctx):
        hdir = os.path.join(os.environ['INRS_LXT'], 'FakeDLL/_run', Util.addBuildPath(r'External'))
        hdir = os.path.normpath(hdir)
        __inc = [ hdir ]
        __lpt = [ ]
        __lib = [ ]
        self.extendCtx(ctx, __lpt, __lib, __inc)

    def configure(self, ctx, tool, version, plateform):
        # C'est le link global qui contrôle le besoin de FakeDLL
        if (ctx.lnk_g == 'static'):
            if (plateform in ['win32', 'win64', 'win64i8']):
                self.__cnf_sta_win(ctx)
            else:
                self.__cnf_sta_unx(ctx)
        else:
            if (plateform in ['win32', 'win64', 'win64i8']):
                self.__cnf_dyn_win(ctx)
            else:
                self.__cnf_dyn_unx(ctx)

