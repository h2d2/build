:: Copie les fichiers des répertoires de compilation vers
:: le répertoire Debug ou Release qui regroupe les fichiers.
::
:: Si utilisé par un autre fichier de commande, utiliser
:: un appel avec call

@echo off
:: ---  Les paramètres
set PRJ_ROOT=%~f1
set SRC_CONF=%2
set PTF_CONF=%3

:: ---  Contrôles
if /I [%PTF_CONF%] == [win32] goto Plateform_OK
if /I [%PTF_CONF%] == [x64]   goto Plateform_OK
@echo Error: Invalid plateform: %PTF_CONF%
goto error
:Plateform_OK

:: ---  Les différents répertoires destination
set DST_DIR=%INRS_DEV%\%SRC_CONF%\%PTF_CONF%
set DST_BIN_DIR=%DST_DIR%\bin
set DST_DOC_DIR=%DST_DIR%\doc
set DST_ETC_DIR=%DST_DIR%\etc
set DST_LFR_DIR=%DST_DIR%\locale\fr
set DST_LEN_DIR=%DST_DIR%\locale\en
set DST_SCR_DIR=%DST_DIR%\script

:: ---  Les différents répertoires source
if /I [%PTF_CONF%] == [win32] set SRC_BIN_DIR=%PRJ_ROOT%\%SRC_CONF%
if /I [%PTF_CONF%] == [x64]   set SRC_BIN_DIR=%PRJ_ROOT%\%PTF_CONF%\%SRC_CONF%
set SRC_CMD_DIR=%PRJ_ROOT%\..\bin
set SRC_DOC_DIR=%PRJ_ROOT%\..\doc
set SRC_ETC_DIR=%PRJ_ROOT%\..\etc
set SRC_LFR_DIR=%PRJ_ROOT%\..\locale\fr
set SRC_LEN_DIR=%PRJ_ROOT%\..\locale\en
set SRC_SCR_DIR=%PRJ_ROOT%\..\script
set SRC_SQL_DIR=%PRJ_ROOT%\..\

:: ---  La commande
set COPY_CMD=copy
echo Copying files to %DST_DIR%

:: ---  Copie les fichiers d'initialisation
if exist %SRC_ETC_DIR% for /R %SRC_ETC_DIR% %%f in ("*.ini") do echo %%f && %COPY_CMD% %%f %DST_BIN_DIR% 1>NUL

:: ---  Copie les fichiers de configuration de H2D2
if exist %SRC_ETC_DIR% for /R %SRC_ETC_DIR% %%f in ("*.h2d2-*")  do echo %%f && %COPY_CMD% %%f %DST_ETC_DIR% 1>NUL
if exist %SRC_ETC_DIR% for /R %SRC_ETC_DIR% %%f in ("*.tides-*") do echo %%f && %COPY_CMD% %%f %DST_ETC_DIR% 1>NUL
if exist %SRC_ETC_DIR% for /R %SRC_ETC_DIR% %%f in ("tide3.dat") do echo %%f && %COPY_CMD% %%f %DST_ETC_DIR% 1>NUL

:: ---  Copie les fichiers *.hlp
if exist %SRC_ETC_DIR% for /R %SRC_ETC_DIR% %%f in ("*.hlp") do echo %%f && %COPY_CMD% %%f %DST_DOC_DIR% 1>NUL
if exist %SRC_DOC_DIR% for /R %SRC_DOC_DIR% %%f in ("*.hlp") do echo %%f && %COPY_CMD% %%f %DST_DOC_DIR% 1>NUL

:: ---  Copie les fichiers *.html
if exist %SRC_ETC_DIR% for /R %SRC_ETC_DIR% %%f in ("*.htm*") do echo %%f && %COPY_CMD% %%f %DST_DOC_DIR% 1>NUL
if exist %SRC_DOC_DIR% for /R %SRC_DOC_DIR% %%f in ("*.htm*") do echo %%f && %COPY_CMD% %%f %DST_DOC_DIR% 1>NUL
if exist %SRC_DOC_DIR% for /R %SRC_DOC_DIR% %%f in ("*.css")  do echo %%f && %COPY_CMD% %%f %DST_DOC_DIR% 1>NUL
if exist %SRC_DOC_DIR% for /R %SRC_DOC_DIR% %%f in ("*.js")   do echo %%f && %COPY_CMD% %%f %DST_DOC_DIR% 1>NUL
if exist %SRC_DOC_DIR% for /R %SRC_DOC_DIR% %%f in ("*.gif")  do echo %%f && %COPY_CMD% %%f %DST_DOC_DIR% 1>NUL

:: ---  Copie les fichiers *.lst
if exist %SRC_ETC_DIR% for /R %SRC_ETC_DIR% %%f in ("*.lst") do echo %%f && %COPY_CMD% %%f %DST_ETC_DIR% 1>NUL

:: ---  Copie les locale
if exist %SRC_LFR_DIR% for /R %SRC_LFR_DIR% %%f in ("*.stbl") do echo %%f && %COPY_CMD% %%f %DST_LFR_DIR% 1>NUL
if exist %SRC_LEN_DIR% for /R %SRC_LEN_DIR% %%f in ("*.stbl") do echo %%f && %COPY_CMD% %%f %DST_LEN_DIR% 1>NUL

:: ---  Copie les executables
if exist %SRC_BIN_DIR% for /R %SRC_BIN_DIR% %%f in ("*.dll") do echo %%f && %COPY_CMD% %%f %DST_BIN_DIR% 1>NUL
if exist %SRC_BIN_DIR% for /R %SRC_BIN_DIR% %%f in ("*.lib") do echo %%f && %COPY_CMD% %%f %DST_BIN_DIR% 1>NUL
if exist %SRC_BIN_DIR% for /R %SRC_BIN_DIR% %%f in ("*.exe") do echo %%f && %COPY_CMD% %%f %DST_BIN_DIR% 1>NUL
if exist %SRC_BIN_DIR% for /R %SRC_BIN_DIR% %%f in ("*.pdb") do echo %%f && %COPY_CMD% %%f %DST_BIN_DIR% 1>NUL
if exist %SRC_BIN_DIR% for /R %SRC_BIN_DIR% %%f in ("*.map") do echo %%f && %COPY_CMD% %%f %DST_BIN_DIR% 1>NUL
if exist %SRC_BIN_DIR% for /R %SRC_BIN_DIR% %%f in ("*.intermediate.manifest") do echo %%f && %COPY_CMD% %%f %DST_BIN_DIR% 1>NUL
if exist %SRC_CMD_DIR% for /R %SRC_CMD_DIR% %%f in ("*.bat") do echo %%f && %COPY_CMD% %%f %DST_BIN_DIR% 1>NUL
if exist %SRC_SCR_DIR% for /R %SRC_SCR_DIR% %%f in ("*.bat") do echo %%f && %COPY_CMD% %%f %DST_BIN_DIR% 1>NUL

:: ---  Copie les scripts
if exist %SRC_SCR_DIR% for /R %SRC_SCR_DIR% %%f in ("*.py")  do echo %%f && %COPY_CMD% %%f %DST_SCR_DIR% 1>NUL
if exist %SRC_BIN_DIR% for /R %SRC_BIN_DIR% %%f in ("*.pyd") do echo %%f && %COPY_CMD% %%f %DST_SCR_DIR% 1>NUL
if exist %SRC_SCR_DIR% for /R %SRC_SCR_DIR% %%f in ("*.xml") do echo %%f && %COPY_CMD% %%f %DST_SCR_DIR% 1>NUL
if exist %SRC_SQL_DIR% for /R %SRC_SQL_DIR% %%f in ("*.sql") do echo %%f && %COPY_CMD% %%f %DST_SCR_DIR% 1>NUL

:: ---  Copie les images
if exist %SRC_SCR_DIR% for /R %SRC_SCR_DIR% %%f in ("*.jpg") do echo %%f && %COPY_CMD% %%f %DST_SCR_DIR% 1>NUL

:: ---  Nettoie les erreurs
:error
exit /b 0
