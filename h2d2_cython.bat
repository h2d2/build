:: Setup
@echo off
setlocal EnableExtensions EnableDelayedExpansion

:: ---  Count args and transfer to arrays
echo ----------------------------
set argCountPP=0
set argCountDD=0
set "allArgs=%*"
:loop
   for /f "tokens=1*" %%x in ("%allArgs%") do (
      call set dd=%%x
      if /i "!dd:~0,2!"=="--" (
         set /A argCountDD+=1
         set "argVecDD[!argCountDD!]=%%~x"
      ) else (
         set /A argCountPP+=1
         set "argVecPP[!argCountPP!]=%%~x"
      )
      set allArgs=%%y
   )
if defined allArgs goto :loop
::echo %argCountDD% !argCountPP!
::for /L %%i in (1,1,%argCountDD%) do (
::   echo %%i- !argVecDD[%%i]!
::)

:: ---  Get DD (double dash) input params
set H2D2_BUILD=release
set H2D2_LIBDIR=%INRS_DEV%/Release/x64/bin build_ext
for /L %%i in (1,1,%argCountDD%) do (
   if /i "!argVecDD[%%i]:~0,13!"=="--h2d2-build="  (
      call :split_string "!argVecDD[%%i]!" "=" & set H2D2_BUILD=!RET_2!
   ) else if /i "!argVecDD[%%i]:~0,14!"=="--h2d2-libdir=" (
      call :split_string "!argVecDD[%%i]!" "=" & set H2D2_LIBDIR=!RET_2!
   ) else (
      goto :usage
   )
)
:: ---  Get PP (positional) input params
set PTH_PXD=!argVecPP[1]!
if [!argVecPP[2]!]==[] ( set DIR_OUT="." ) else ( set DIR_OUT=%CD%/!argVecPP[2]! )
if [!argVecPP[3]!]==[] ( set DIR_BLD="__cython-build" ) else ( set DIR_BLD=%CD%/!argVecPP[3]! )

:: ---  Transform .pxd in .setup.py
call :split_path %PTH_PXD% & set FIC_PXD=!RET_1! & set DIR_INP=!RET_2!
set DIR_BAT=%~dp0

:: ---  Unix-ify the paths to get rid of \ problems
call set DIR_OUT=%%DIR_OUT:\=/%%
call set DIR_BLD=%%DIR_BLD:\=/%%
call set DIR_BAT=%%DIR_BAT:\=/%%

:: ---  Compile to pyd
pushd %DIR_INP%
:: cython 0.28.2
:: cython-c-in-temp pyrex-c-in-temp font que la cython ne prend pas
:: en compte le fichier .pxd et la compilation n'est pas optimisée.
set CYTHON_CMD=python %DIR_BAT%/h2d2_cython.py %FIC_PXD% build_ext --build-lib=%DIR_OUT% --build-temp=%DIR_BLD% --h2d2-build=%H2D2_BUILD% --h2d2-libdir=%H2D2_LIBDIR%
echo %CYTHON_CMD%
%CYTHON_CMD%
popd
goto :EOF

:split_string
   setlocal EnableExtensions EnableDelayedExpansion
   SET _STR=%~1
   SET _TOK=%~2
   for /F "tokens=1* delims=%_TOK%" %%x in ("%_STR%") do (
      call set _LHS=%%x
      call set _RHS=%%y
   )
   endlocal & set RET_1=%_LHS% & set RET_2=%_RHS%
goto :EOF

:split_path
   setlocal EnableExtensions EnableDelayedExpansion
   set _NAM=%~nx1
   set _DIR=%~dp1
   set _DIR=%_DIR:~0,-1%
   endlocal & set RET_1=%_NAM% & set RET_2=%_DIR%
goto :EOF

:usage
   echo Usage:
   echo    h2d2_cython.bat full_path_to_pxd_file [--h2d2-build=(debug,release)] [--h2d2-libdir=path_to_h2d2_binary_files]
goto exit

:exit