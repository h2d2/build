@echo off
set TGT_PTF=%1

goto intel
::goto msvc

:intel
if /I ["%TGT_PTF%"] == ["win32"] (
    set TGT_CPL=itlX86
) else (
    set TGT_CPL=itlX64
)
set TGT_CPL_VER=23.2
goto EOF

:msvc
set TGT_CPL=msvc
set TGT_CPL_VER=90
goto EOF

:EOF