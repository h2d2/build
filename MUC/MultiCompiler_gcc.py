# -*- coding: UTF-8 -*-
#-----------------------------------------------------------------------------------------------------
# I.N.R.S. - Processus de compilation multi-compilateur
#
#   Compilateur gcc
#-----------------------------------------------------------------------------------------------------

import MUC_platform

import glob
import os
import shutil
import subprocess

def isFreeForm(ext):
    return ext.lower() not in ['.f', '.for']

"""
GNU - Configuration
"""
class cfg_bse:
    def getCfg_c(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_bse = [
                    '-c',
                    #'-E',  # Precompile only
                    '-Wall',
                    '-W',
                    '-fkeep-inline-functions',
                    #'-fvisibility=hidden',      # Change default visibility for shared-object
                    #'-finstrument-functions ',  #
                    ]
        flags_cfg = {
                    'debug'     : ['-O0', '-g3', '-fno-inline', '-ggdb'],
                    'profil'    : ['-O0', '-g3', '-fno-inline', '-pg'],
                    'release'   : ['-O3', '-g']
                    }
        return flags_bse + flags_cfg[c]

    def getLnk_c(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        if   (p == 'win32' or p == 'win64'):
            flags_lnk = {
                         'debug' :
                           {
                              'static' :  [ ],
                              'dynamic' : [ ],
                           },
                         'profil' :
                           {
                              'static' :  [ ],
                              'dynamic' : [ ],
                           },
                         'release' :
                           {
                              'static' :  [ ],
                              'dynamic' : [ ],
                           },
                        }
        elif (p == 'unx32' or p == 'unx64'):
            flags_lnk = {
                         'debug' :
                           {
                              'static' :  [ ],
                              'dynamic' : [ '-fPIC' ],
                           },
                         'profil' :
                           {
                              'static' :  [ ],
                              'dynamic' : [ '-fPIC' ],
                           },
                         'release' :
                           {
                              'static' :  [ ],
                              'dynamic' : [ '-fPIC' ],
                           },
                        }
        else:
            raise RuntimeError('Invalid gcc platform: %s not in [win32, win64, unx32, unx64]' % p)
        return flags_lnk[c][l]

    def getPtf_c(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        def getIncludePath():
            if (MUC_platform.is_win()):
                # MinGW ne cherche pas des include comme <float.h> au bon endroit
                for p in os.environ['PATH'].split(';'):
                    if os.path.isfile( os.path.join(p, 'gcc.exe') ):
                        return p + '/../include'
                raise RuntimeError('Could not find gcc.exe in PATH')
            else:
                return 'ERROR:_include_path_only_valid_on_windows_mingw_platform'
        flags_ptf = {
                     'unx32' : [ '-m32' ],
                     'unx64' : [ '-m64' ],
                     'win32' : [ '-m32' ], #, '-isystem "%s"' % getIncludePath() ],
                     'win64' : [ '-m64' ], #, '-isystem "%s"' % getIncludePath() ],
                     }
        return flags_ptf[p]

    def c_cfg(self, cfg):
        return self.cpp_cfg(cfg)

    def cpp_cfg(self, cfg):
        return self.getCfg_c(cfg) + self.getLnk_c(cfg) + self.getPtf_c(cfg)

class cfg_cpp_3x(cfg_bse):
    def getCfg_c(self, cfg):
        flags_bse = [
                    '-std=c++17' if cfg.fic_typ == 'cpp' else '',   # required for unordered_map
                    ]
        return cfg_bse.getCfg_c(self, cfg) + flags_bse

class cfg_cpp_40(cfg_cpp_3x):
    def getCfg_c(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_cfg = {
                    'debug'     : [],
                    'profil'    : [],
                    'release'   : []
                    }
        return cfg_cpp_3x.getCfg_c(self, cfg) + flags_cfg[c]

class cfg_cpp_4x(cfg_cpp_40):
    def getCfg_c(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_cfg = {
                    'debug'     : [],
                    'profil'    : [],
                    'release'   : ['-fopenmp']
                    }
        return cfg_cpp_40.getCfg_c(self, cfg) + flags_cfg[c]


class cfg_ftn_3x(cfg_bse):
    def getCfg_f(self, cfg):
        flags_bse = [
                    '-Wno-unused',
                    '-fno-f2c',
                    '-fno-second-underscore',
                    '-fdollar-ok',
                    #'-finstrument-functions ',
                    '-fdec-structure',
                    '-J' + cfg.mod_dir,
                    '-I' + cfg.mod_dir,
                    ]
        return cfg_bse.getCfg_c(self, cfg) + flags_bse

    def getLnk_f(self, cfg):
        return cfg_bse.getLnk_c(self, cfg)

    def getPtf_f(self, cfg):
        return cfg_bse.getPtf_c(self, cfg)

    def ftn_cfg(self, cfg):
        return self.getCfg_f(cfg) + self.getLnk_f(cfg) + self.getPtf_f(cfg)


class cfg_ftn_40(cfg_ftn_3x):
    def getCfg_f(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_cfg = {
                    'debug'     : [ ] if isFreeForm(cfg.fic_ext) else ['-fd-lines-as-code'],
                    'profil'    : [ ] if isFreeForm(cfg.fic_ext) else ['-fd-lines-as-comments'],
                    'release'   : [ ] if isFreeForm(cfg.fic_ext) else ['-fd-lines-as-comments']
                    }
        flags_isz = {
                    4 : [ ],
                    8 : ['-fdefault-integer-8'],
                    }
        return cfg_ftn_3x.getCfg_f(self, cfg) + flags_cfg[c] + flags_isz[i]

class cfg_ftn_4x(cfg_ftn_40):
    def getCfg_f(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_bse = [
                    '-fno-range-check',
                    '-ffree-line-length-none',  # unlimited length
                    '-cpp',
                    ]
        flags_cfg = {
                    'debug'     : [ ],
                    'profil'    : [ ],
                    'release'   : [ '-fopenmp' ]
                    }
        return cfg_ftn_40.getCfg_f(self, cfg) + flags_bse + flags_cfg[c]

class cfg_ftn_10x(cfg_ftn_4x):
    def getCfg_f(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_bse = [
                    '-fallow-argument-mismatch',
                    ]
        return cfg_ftn_4x.getCfg_f(self, cfg) + flags_bse

"""
GNU-gcc générique
"""
class gcc:
    def __fixModules(self, cfg):
        for f in glob.glob( os.path.join(cfg.mod_dir, '*.mod') ):
            nam, ext = os.path.splitext( os.path.basename(f) )
            if nam != nam.upper():
                modSrc = '.'.join( (nam,'mod') )
                modDst = '.'.join( (nam.upper(),'mod') )
                pthSrc = os.path.join(cfg.mod_dir, modSrc)
                pthDst = os.path.join(cfg.mod_dir, modDst)
                if os.path.isfile(pthSrc):
                    if not os.path.isfile(pthDst) or os.path.getmtime(pthSrc) > os.path.getmtime(pthDst):
                        #print 'Copied to: ', pthDst
                        shutil.copyfile(pthSrc, pthDst)
                        shutil.copystat(pthSrc, pthDst)

    def __go(self, cmplr, cfg):
        c = '%s %s %s %s -o%s %s' % (cmplr, ' '.join(cfg.cpl_cfg), ' '.join(cfg.cpl_inc), ' '.join(cfg.cpl_dfn), cfg.fic_out, cfg.fic_inp)

        _stderr = None
        _stdout = None
        _shell  = True
        if (cfg.fic_err):
            _stdout = open(cfg.fic_err, 'w')
            _stderr = subprocess.STDOUT

        if (cfg.dmp_cmd):
            print('--- Compiling on %s for %s(i%s)' % (MUC_platform.build_platform(), cfg.tgt_ptf, cfg.tgt_isz))
            print(c)
            print('-------')

        retcode = subprocess.call(c, stdout=_stdout, stderr=_stderr, shell=_shell)
        self.__fixModules(cfg)

    def cpp(self, cmplr, cfg):
        self.__go(cmplr, cfg)

    def ftn(self, cmplr, cfg):
        self.__go(cmplr, cfg)

"""
GNU 3.x
"""
class gcc_3x(gcc, cfg_cpp_3x, cfg_ftn_3x):
    def __init__(self, v = '-3.3'):
      self.ver = v

    def c(self, cfg):
        gcc.cpp(self, 'gcc%s' % self.ver, cfg)

    def cpp(self, cfg):
        gcc.cpp(self, 'g++%s' % self.ver, cfg)

    def ftn(self, cfg):
        gcc.ftn(self, 'g77%s' % self.ver, cfg)

    def c_cfg(self, cfg):
        return cfg_cpp_3x.c_cfg(self, cfg)

    def cpp_cfg(self, cfg):
        return cfg_cpp_3x.cpp_cfg(self, cfg)

    def ftn_cfg(self, cfg):
        return cfg_ftn_3x.ftn_cfg(self, cfg)

class gcc_33(gcc_3x):
    def __init__(self):
      gcc_3x.__init__(self, '-3.3')

class gcc_34(gcc_3x):
    def __init__(self):
      gcc_3x.__init__(self, '-3.4')

"""
GNU 4.0
"""
class gcc_40(gcc, cfg_cpp_40, cfg_ftn_40):
    def __init__(self, v = '-4.0'):
      self.ver = v

    def c(self, cfg):
        gcc.cpp(self, 'gcc%s' % self.ver, cfg)

    def cpp(self, cfg):
        gcc.cpp(self, 'g++%s' % self.ver, cfg)

    def ftn(self, cfg):
        gcc.ftn(self, 'gfortran%s' % self.ver, cfg)

    def c_cfg(self, cfg):
        return cfg_cpp_40.c_cfg(self, cfg)

    def cpp_cfg(self, cfg):
        return cfg_cpp_40.cpp_cfg(self, cfg)

    def ftn_cfg(self, cfg):
        return cfg_ftn_40.ftn_cfg(self, cfg)

"""
GNU 4.1 - Comme gcc 4.0 à cause de openmp
"""
class gcc_41(gcc_40):
    def __init__(self):
      gcc_40.__init__(self, '-4.1')

"""
GNU 4 générique (sauf 4.0, 4.1)
"""
class gcc_4x(gcc, cfg_cpp_4x, cfg_ftn_4x):
    def __init__(self, v = ''):
      self.ver = v

    def c(self, cfg):
        gcc.cpp(self, 'gcc%s' % self.ver, cfg)

    def cpp(self, cfg):
        gcc.cpp(self, 'g++%s' % self.ver, cfg)

    def ftn(self, cfg):
        gcc.ftn(self, 'gfortran%s' % self.ver, cfg)

    def c_cfg(self, cfg):
        return cfg_cpp_4x.c_cfg(self, cfg)

    def cpp_cfg(self, cfg):
        return cfg_cpp_4x.cpp_cfg(self, cfg)

    def ftn_cfg(self, cfg):
        return cfg_ftn_4x.ftn_cfg(self, cfg)

"""
GNU 4
"""
class gcc_42(gcc_4x):
    def __init__(self):
      gcc_4x.__init__(self, '-V4.2')

class gcc_43(gcc_4x):
    def __init__(self):
      gcc_4x.__init__(self, '-4.3')

class gcc_44(gcc_4x):
    def __init__(self):
      gcc_4x.__init__(self, '-4.4')

class gcc_45(gcc_4x):
    def __init__(self):
      gcc_4x.__init__(self, '-4.5')

class gcc_46(gcc_4x):
    def __init__(self):
      gcc_4x.__init__(self, '-4.6')

"""
GNU 9
"""
class gcc_9(gcc_4x):
    def __init__(self):
      super(gcc_9, self).__init__('-9')

class gcc_91(gcc_9):
    def __init__(self):
      super(gcc_91, self).__init__('-9.1')

class gcc_92(gcc_9):
    def __init__(self):
      super(gcc_92, self).__init__('-9.2')

class gcc_93(gcc_9):
    def __init__(self):
      super(gcc_93, self).__init__('-9.3')

"""
GNU 10 générique
"""
class gcc_10x(gcc, cfg_cpp_4x, cfg_ftn_10x):
    def __init__(self, v = ''):
      self.ver = v

    def c(self, cfg):
        gcc.cpp(self, 'gcc%s' % self.ver, cfg)

    def cpp(self, cfg):
        gcc.cpp(self, 'g++%s' % self.ver, cfg)

    def ftn(self, cfg):
        gcc.ftn(self, 'gfortran%s' % self.ver, cfg)

    def c_cfg(self, cfg):
        return cfg_cpp_4x.c_cfg(self, cfg)

    def cpp_cfg(self, cfg):
        return cfg_cpp_4x.cpp_cfg(self, cfg)

    def ftn_cfg(self, cfg):
        return cfg_ftn_10x.ftn_cfg(self, cfg)


"""
GNU 12 générique
"""
class gcc_12x(gcc_10x):
    def __init__(self, v = ''):
      self.ver = v

class gcc_12(gcc_12x):
    def __init__(self):
      super().__init__('-12')

"""
GNU version par défaut, mais de la série 10.
"""
class gcc_xx(gcc_10x):
    def __init__(self):
      super().__init__(' ')

