# -*- coding: UTF-8 -*-
'''
cgi-bin

'''

import EW_HtmlErWr
import os
import string
import sys

arg_date = '*'
arg_xvar = 'date'
arg_xval = '*'
arg_yvar = 'global'
arg_yval = '*'

def erreur(msg, status='500 Internal Server Error'):
    print('Status:', status)
    print()
    print(msg)
    sys.exit(0)

def parseEnvString():
    global arg_date, arg_xvar, arg_xval, arg_yvar, arg_yval

    inp = os.getenv("QUERY_STRING")
    if (inp == None or inp == ""): inp = "date=*&package=*&global=*"
    tokens = inp.split("&")

    stok = string.split(tokens[0], "=")
    arg_date = stok[1]
    stok = string.split(tokens[1], "=")
    arg_xvar = string.upper(stok[0][0]) + string.lower(stok[0][1:])
    arg_xval = stok[1]
    stok = string.split(tokens[2], "=")
    arg_yvar = string.upper(stok[0][0]) + string.lower(stok[0][1:])
    arg_yval = stok[1]

def main():
    global arg_date, arg_xvar, arg_xval, arg_yvar, arg_yval

    path = "/var/www/intranet/logs/CI"
    #path = "e:/dev"

    #---  Parse les info
    parseEnvString()

    #--- Recupere le nom du logiciel
    reps = arg_xval
    pathEntier = os.path.join(path, reps)
    pathEntier = os.path.normpath(pathEntier)

    #---  Assemble la liste des dates
    dates = []
    for f in os.listdir(pathEntier):
        if (f[0:3] != "EW_"): continue
        if (os.path.splitext(f)[1] != ".xml"): continue
        date = os.path.splitext(f)[0]
        date = date[3:]
        dates.append(date)
    dates.sort()
    dates.reverse()

    #---  Date par défaut
    if (len(dates) > 0):
        if (arg_date == "*"): arg_date = dates[0]

    #---  Construis l'algo d'ecriture
    if (len(dates) > 0):
        algo = EW_HtmlErWr.EWGenerique______()
        algo.xeq(pathEntier, dates, arg_date, arg_xvar, arg_xval, arg_yvar, arg_yval)

if __name__ == '__main__':
    main()

