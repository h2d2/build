# -*- coding: UTF-8 -*-
'''
Scan a directory tree to parse the errors and warning.
Dumps the result as an xml file

Usage:
    EWParse what input_path output_path

Where:
    what            [Tree, Errors]
    input_path      Root path of the file tree that will be scanned
    output_path     Output Path where to write the result file
'''

from EW_Info import EWInfoDate
from EW_Tree import EWNodeTree
import os
import sys
import time
from EW_Util import dbg

def parseErrWrn(inpPath, outPath):
    dbg.on = 0
    date = EWInfoDate()
    date.xtrInfo(inpPath)

    nomFic = os.path.join(outPath, "EW_" + date.nom + ".xml")
    date.ecrisXML(nomFic)

def parseDepTree(inpPath, outPath):
    tree = EWNodeTree()
    tree.readTree(inpPath)
    tree.linkNodes()

    date = time.strftime("%Y%m%d-%H%M%S")
    nomFic = os.path.join(outPath, "DT_" + date + ".xml")
    tree.writeXML(nomFic)


def main():

    def Usage():
        print(__doc__)
        sys.exit(1)

    if (len(sys.argv) != 4): Usage()

    what = sys.argv[1]
    inpPath = sys.argv[2]
    outPath = sys.argv[3]

    ret = 0
    if (what.lower() == "tree"):
        parseDepTree(inpPath, outPath)
    elif(what.lower() == "errors"):
        parseErrWrn (inpPath, outPath)
    else:
        ret = 1

    return ret


if __name__ == '__main__':
    status = main()
    sys.exit(status)

