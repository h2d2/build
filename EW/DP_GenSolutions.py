# -*- coding: UTF-8 -*-
import fileinput
import os
import stat
import string
import sys
import tempfile
import tokenize
from xml.dom import minidom
from string import split, replace

def traite(dir):
    if (not xeqAction(dir)):
        xeqRecursion(dir)

def xeqRecursion(dir):
    for f in os.listdir(dir):
        if os.path.isdir(os.path.join(dir,f)):
            if (dir == "."):
                traite(f)
            else:
                traite(os.path.join(dir,f))


def xeqAction(dir):
    path = os.path.join(dir, "prjVisual")
    res = os.path.isdir(path)
    if (res):
        path = replace(dir,"\\","/")
        module = split(path, "/")[-1]
        if (module != "main"):
            print("Traite: %s" % module)
            print(" Module path: %s" % path)
            genereSolution(module, path)
        #    try:
        #        if (os.path.isfile(path + "/prjVisual/"  + module + ".sln.$$$")):
        #            os.remove(path + "/prjVisual/" + module + ".sln")
        #            os.rename(path + "/prjVisual/"  + module + ".sln.$$$", path + "/prjVisual/" + module + ".sln")
        #    except OSError:
        #        pass
    return res

def lisFichierInclude(module, pathfichier):
    includes = [("", "")]
    curpath = os.getcwd().replace("\\", "/")
    if (len(curpath) > 0 and curpath[0].isupper()):
        curpath = curpath.replace(curpath[0] + ":", curpath[0].lower() + ":")
    #Nmodule = module
    nomMod = ""
    pathtmp = ""
    sspath = ""
    other = ""
    try:
        for line in fileinput.FileInput(pathfichier):
            lenline = len(line)
            if (lenline > 5 and line[3:5].lower() == "e:"):

                if (lenline > 2 and line[0:2] == "-I"):
                    line = line[2:]
                    line = line.lstrip()
                    lenline = lenline - 2

                if (lenline > 0 and line[0] == "\""):
                    line = line[1:]
                    line = line.lstrip()
                    lenline = lenline - 1

                if (lenline > 0 and line[-1] == "\n"):
                    line = line[0:-1]
                    line = line.rstrip()
                    lenline = lenline - 1

                if (lenline > 0 and line[-1] == "\""):
                    line = line[0:-1]
                    line = line.rstrip()
                    lenline = lenline - 1

                if (lenline > 0):
                    pathtmp, nomMod = os.path.split(os.path.dirname(line))
                    sspath = nomMod
                    if (len(pathtmp) > 0 and pathtmp[0].isupper()):
                        pathtmp = pathtmp.replace(pathtmp[0] + ":", pathtmp[0].lower() + ":")


                    while pathtmp != curpath:
                        pathtmp, other = os.path.split(pathtmp)
                        sspath = other + "\\" + sspath

                    if (nomMod == module):
                        includes[0] = (nomMod, sspath)
                    else:
                        includes.append((nomMod, sspath))
    except IOError:
        pass

    #includes.sort()
    return includes

def creeBackup(fichier, backup):
    try:
        if (stat.S_ISREG(os.stat(fichier)[stat.ST_MODE])):
            try:
                if (stat.S_ISREG(os.stat(backup)[stat.ST_MODE])):
                    print(" Overwriting existing file %s" % backup)
                    os.remove(backup)
            except OSError:
                pass
            print(" Creating backup file %s" % backup)
            os.rename(fichier, backup)
    except OSError:
        pass

def calculNbSsPath(pathmod):
    path = ""
    #print pathmod
    while pathmod != "":
        pathmod, mod = os.path.split(pathmod)
        path = "..\\" + path
    #print path
    return path

def numSerieMod(absPath, nomMod):
    file= absPath.replace("/", "\\") + "\\" + "prjVisual\\" + nomMod + ".vcproj"
    #print file
    res = ""
    if (os.path.isfile(file)):
        xmldoc = minidom.parse(file)
        #xmlparser.ParseFile(rPath)
        nodeList = xmldoc.getElementsByTagName("VisualStudioProject")
        if (len(nodeList) > 0):
            node = nodeList[0]
            res = "\"" + node.getAttribute("ProjectGUID") + "\""
    #print res
    return res

def detecteEtMajNouvMod(fileNew, modules, numproj, pathmod):
    listeSerie = []
    lenmod = len(modules)
    pathR = calculNbSsPath(pathmod)
    rPath = ""
    cpt = 0
    res = ""
    while cpt < lenmod:
        nomMod, nomModComplet = modules[cpt]
        if (nomMod != ""):
            #il faut ajouter le chemin du module à la solution
            rPath = pathR + nomModComplet + "\\" + "prjVisual\\" + nomMod + ".vcproj"
            # On va chercher le vcproj dans le but d'obtenir le numéro de série du projet.
            vcprojnumserie = numSerieMod(nomModComplet, nomMod)
            if (vcprojnumserie != ""):
                res = res + "Project(%s) = \"%s\", \"%s\", %s\n" % (numproj, nomMod, rPath, vcprojnumserie)
                res = res + "EndProject\n"
                listeSerie.append(vcprojnumserie)
        cpt = cpt + 1
    res = res + "Global\n"
    fileNew.write(res)
    return listeSerie

def verifieModule(curMod, modules, lenmodule):
    cpt = 0
    #for mod in modules:
    mod = ""
    modComplet = ""
    #print "Nom du module a comparer: " + curMod
    while cpt < lenmodule and mod != curMod:
        mod, modComplet = modules[cpt]
        mod = "\"" + mod + "\""
        #print mod
        cpt = cpt + 1
    if (mod == curMod and cpt > 0):
        cpt = cpt - 1
    #print cpt
    return cpt

def numSerieMainProj(tokens, fileNew):
    cptstring = 0
    numSerieVcproj = ""
    numSerieProj = ""
    for tok, val, start, end, line in tokens:
        if (tok == tokenize.STRING):
            if (cptstring == 0):
                numSerieProj = val
            if (cptstring == 3):
                numSerieVcproj = val
                # On sauve la ligne
                fileNew.write(line + "EndProject\n")
                break
            cptstring = cptstring + 1
        #elif (tok == tokenize.NEWLINE): break
    return numSerieProj, numSerieVcproj

def detecteEtMajModules(tokens, fileNew, modules, nomMod, pathmod):
    lenmodule = len(modules)
    listVcprojSerie = []
    cptstring = 0
    cptlenmodule = lenmodule
    res = ""
    numproj = ""
    # On sauve d'abord le numéro de série de la solution.
    #numserie = numSerieMod(pathmod, nomMod)
    #print numserie
    #numproj, numserie = numSerieMainProj(tokens, fileNew)
    pathmod = pathmod + "/prjVisual"
    # On vérifie et on met à jour la liste des modules disponibles dans la solution.
    for tok, val, start, end, line in tokens:
        if (tok != tokenize.OP and tok != tokenize.NEWLINE):
            if (tok == tokenize.STRING):
                if (numproj == "" and cptstring == 0): numproj = val
                if (cptstring == 1) :
                    cptlenmodule = verifieModule(val, modules, lenmodule)
                    if (cptlenmodule != lenmodule):
                        #print "il faut sauver la ligne"
                        if (cptlenmodule == 0):
                            res = line + "EndProject\n" + res
                        else:
                            res = res + line +"EndProject\n"
                        modules[cptlenmodule] = ("", 0)
                        cptlenmodule = 0
                    elif (val[1:4] == "PY_"):
                        res = res + line
                        res = res + "EndProject\n"
                        cptlenmodule = 0
                elif (cptstring == 3):
                    cptstring = -1
                    if (cptlenmodule == lenmodule):
                        #print val
                        cptlenmodule = 0
                        listVcprojSerie.append(val)
                cptstring = cptstring + 1
            elif (tok == tokenize.INDENT): break
    fileNew.write(res)
    listNouveauVcproj = detecteEtMajNouvMod(fileNew, modules, numproj, pathmod)
    sauveLigne(tokens, fileNew)
    return listVcprojSerie, listNouveauVcproj

def estDansListe(listVcprojSerie, val):
    for vcproj in listVcprojSerie:
        if (vcproj == val):
            return True
    return False

def reqNumSerie(tokens):
    numserie = "{"
    for tok, val, start, end, line in tokens:
        if (val == "}"):
            numserie = numserie + "}"
            break
        else:
            numserie = numserie + val
    # On ignore les tokens .0={
    for tok, val, start, end, line in tokens:
        if (val == "{" or tok == tokenize.NEWLINE):
            break
    return numserie


def majDependance(tokens, fileNew, listVcprojSerie, numSerieProj):
    sauveLigne(tokens, fileNew)
    modQuiDepend = False
    oldValProj = ""
    curValProj = ""
    curValMod = ""
    res = ""
    nbDpProj = 0
    nbDp = 0
    numSeriePj = numSerieProj.replace("\"", "")
    #print numSeriePj
    for tok, val, start, end, line in tokens:
        if (tok != tokenize.INDENT and tok != tokenize.NEWLINE):
            if (val == "EndGlobalSection"): break
            #On vérifie ici que le numéro de série du module est bien encore utilisé.
            # Si modQuiDepend est vrai alors cela veut dire que le module n'est plus utilisé(liste des modules plus utilisés)
            if (val == "{"):
                curValProj = reqNumSerie(tokens)
                if (curValProj != oldValProj):
                    if (oldValProj == numSeriePj):
                        nbDpProj = nbDpProj + nbDp
                    nbDp = 0
                    #print curValProj
                    #print line
                    modQuiDepend = estDansListe(listVcprojSerie, "\"" + curValProj + "\"")

                # Si le module n'appartient pas à la liste des modules inutilisés alors on vérifie que le numéro de série de la dépendance
                # est encore utilisé.
                if (not modQuiDepend):
                    curValMod = reqNumSerie(tokens)
                    #print curValMod
                    if (not estDansListe(listVcprojSerie, "\""+ curValMod + "\"")):
                        # Les dépendances ici sont correctes. On peut sauver la ligne.
                        #print line
                        res = res + "\t\t" + curValProj + "." + str(nbDp) + " = " + curValMod + "\n"
                        nbDp = nbDp + 1
                oldValProj = curValProj
    if (oldValProj == numSeriePj):
        nbDpProj = nbDpProj + nbDp
    if (res != ""):
        fileNew.write(res)
    return nbDpProj

def majConfiguration(tokens, fileNew, listVcprojSerie, nbconf):
    sauveLigne(tokens, fileNew)
    oldValMod = ""
    curValMod = ""
    modSuppr = False
    res = ""
    #print listVcprojSerie
    # On met à jour suivant les modules utilisés leurs configurations.
    for tok, val, start, end, line in tokens:
        if (tok != tokenize.INDENT and tok != tokenize.NEWLINE):
            if (val == "EndGlobalSection"): break
            if (val == "{"):
                curValMod = reqNumSerie(tokens)
                #print curValMod
                if (curValMod != oldValMod):
                    modSuppr = estDansListe(listVcprojSerie, "\"" + curValMod + "\"")
                if (not modSuppr):
                    # On sauve la ligne
                    #print curValMod
                    res = res + line
                oldValMod = curValMod
    fileNew.write(res)

def nouvConfiguration(fileNew, listVcproj, lstConfig, lenlstConfig):
    lenlst = len(listVcproj)
    cptNouvConf = 1
    cptConf = 0
    mode = ""
    cstnum = ""
    res = ""
    #print listVcproj
    while cptNouvConf < lenlst:
        cstnum = "\t\t" + listVcproj[cptNouvConf].replace("\"", "") + "."
        while cptConf < lenlstConfig:
            mode = lstConfig[cptConf]
            res = res + cstnum + mode + "." + "ActiveCfg = " + mode + "|Win32\n"
            res = res + cstnum + mode + "." + "Build.0 = " + mode + "|Win32\n"
            cptConf = cptConf + 1
        cptConf = 0
        cptNouvConf = cptNouvConf + 1
    if (res != ""):
        fileNew.write(res)

def nouvDependance(fileNew, listVcproj, nbdependance, numserie):
    lenlst = len(listVcproj)
    cpt = 1
    res = ""
    if (lenlst > 1):
        while cpt < lenlst:
            res = res + "\t\t" + numserie.replace("\"", "") + "." + str(nbdependance) + " = "
            res = res + listVcproj[cpt].replace("\"", "") + "\n"
            nbdependance = nbdependance + 1
            cpt = cpt + 1
    if (res != ""):
        fileNew.write(res)

def sauveLigne(tokens, fileNew):
    linesauve = ""
    for tok, val, start, end, line in tokens:
        if (tok == tokenize.NEWLINE):
            linesauve = linesauve + line
            break
    fileNew.write(linesauve)
    #print "On a sauve lentete"

def sauveSolutionConfig(tokens, fileNew):
    linesauve = ""
    config = ""
    lstConfig = []
    for tok, val, start, end, line in tokens:
        if (val == "EndGlobalSection"):
            break
        elif (val == "="):
                for tok, val, start, end, line in tokens:
                    if (tok == tokenize.NEWLINE):
                        linesauve = linesauve + line
                        lstConfig.append(config)
                        config = ""
                        break
                    else:
                        config = config + val
    fileNew.write(linesauve)
    #print lstConfig
    #print "On a sauve la solution config"
    return lstConfig

def sauveFinSolution(tokens, fileNew):
    linesauve = ""
    newline = True
    for tok, val, start, end, line in tokens:
        if (tok == tokenize.NEWLINE):
            linesauve = linesauve + line
    fileNew.write(linesauve)
    #print "On a sauve la fin de la solution"

def genereNouvSol(fileNew, modules, path):

    # On inscrit l'entete du SLN
    fileNew.write("Microsoft Visual Studio Solution File, Format Version 7.00\n");

    # Numéro de la solution
    numproj = "\"{8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942}\""

    # On inscrit les modules
    nom, path = modules[0]
    modules.append(("PY_" + nom, path))
    listNouveauVcproj = detecteEtMajNouvMod(fileNew, modules, numproj, path + "\prjVisual")

    # On genere la configuration de la solution
    fileNew.write("\tGlobalSection(SolutionConfiguration) = preSolution\n\
    \tConfigName.0 = Debug\n\
    \tConfigName.1 = Release\n\
    EndGlobalSection\n")

    lstConfig = ["Debug", "Release"]

    lenlstConfig = len(lstConfig)

    # On ecrit l'entete des dependances:
    fileNew.write("\tGlobalSection(ProjectDependencies) = postSolution\n")

    #print "On lance nouvDependance"
    nouvDependance(fileNew, listNouveauVcproj, 0, listNouveauVcproj[0])

    # On ecrit la fin d'entete des dependances
    fileNew.write("\tEndGlobalSection\n")

    #On ecrit l'entete de la config
    fileNew.write("\tGlobalSection(ProjectConfiguration) = postSolution\n")

    #print "On lance nouvConfiguration"

    nouvConfiguration(fileNew, listNouveauVcproj, lstConfig, lenlstConfig)

    # On ecrit la fin de la solution
    fileNew.write("\tEndGlobalSection\n\
    GlobalSection(ExtensibilityGlobals) = postSolution\n\
    EndGlobalSection\n\
    GlobalSection(ExtensibilityAddIns) = postSolution\n\
    EndGlobalSection\nEndGlobal\n")
    fileNew.close()

def genereSolaPartirSln(fileNew, source, modules, module, path):

    nomMod, nomModComplet = modules[0]
    nmprj = numSerieMod(nomModComplet, nomMod)

    fp = fileinput.FileInput(source)

    # On passe l'entete du fichier sln
    entete = fp.readline()
    fileNew.write(entete)

    # On demarre le generateur de token
    tokens = tokenize.generate_tokens(fp.readline)
    listVcprojSerie, listNouveauVcproj = detecteEtMajModules(tokens, fileNew, modules, module, path)

    #print "On sort de detecteEtMajModules"

    lstConfig = sauveSolutionConfig(tokens, fileNew)

    sauveLigne(tokens, fileNew)

    lenlstConfig = len(lstConfig)

    nbdependance = majDependance(tokens, fileNew, listVcprojSerie, nmprj)

    #print "On lance nouvDependance"

    nouvDependance(fileNew, listNouveauVcproj, nbdependance, nmprj)

    sauveLigne(tokens, fileNew)

    #print "On lance majConfiguration"
    majConfiguration(tokens, fileNew, listVcprojSerie, (lenlstConfig)* 2 - 1)

    #print "On lance nouvConfiguration"

    nouvConfiguration(fileNew, listNouveauVcproj, lstConfig, lenlstConfig)

    sauveFinSolution(tokens, fileNew)
    #for line in modules:
    #    print line
    #fileNew.write("%s = derniere\n" % line)
    fileNew.close()

def trouveInc(path):
    buildPath = path + "/build"
    for f in os.listdir(buildPath):
        filepath = os.path.join(buildPath,f)
        if os.path.isfile(filepath):
            if (f[-4:len(f)] == ".inc"):
                return filepath
    return ""

def genereSolution(module, path):

    source = path + "/prjVisual/" + module + ".sln"
    sourceBackup = source + ".$$$"
    print(" Working on module %s" % module)

    inc = trouveInc(path)
    if (inc != ""):
        modules = lisFichierInclude(module, inc)
    else:
        modules=[('','')]
    if (modules[0] != ('','')):
        if (os.path.isfile(source)):
            creeBackup(source, sourceBackup)
        fileNew = file(source, 'w')
        try:
            if (os.path.isfile(sourceBackup)):
                print(" Updating %s solution file \n" % source)
                genereSolaPartirSln(fileNew, sourceBackup, modules, module, path)
            else:
                print(" Creating new solution file %s\n" % source)
                genereNouvSol(fileNew, modules, path)
        except IOError:
            pass

if __name__ == '__main__':
   lenargv = len(sys.argv)
   module = ""
   if (lenargv > 1): module = "/" + sys.argv[1]
   sys.exit(traite("." + module))
