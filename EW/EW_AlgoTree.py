# -*- coding: UTF-8 -*-
import EW_Tree
from EW_Tree import EWNodeTree
from EW_AlgoErWr import EWFichierHTML


class EWArbreDependanceInverse:
    def __init__(self):
        self.ficHTML = None

    def createTreeItems(self, root, tree):
        nodeKeys = list(tree.mapNodes.keys())
        nodeKeys.sort()
        self.ficHTML.ecris("   var %s = new WebFXTree('%s');" % (root, root))
        for n in nodeKeys:
            self.ficHTML.ecris("   var %s = new WebFXTreeItem('%s');" % (n, n))

    def addNodeToDisplayTree(self, dtNode, node):
        nodeKeys = list(node.mapNodes.keys())
        nodeKeys.sort()
        for n in nodeKeys:
            if (node.mapNodes[n] != None):
                self.addNodeToDisplayTree(n, node.mapNodes[n])
            self.ficHTML.ecris("   %s.add(%s);" % (n, dtNode))

    def writeProlog(self):
        self.ficHTML.ecris("<script src=\"/js/xtree.js\" language=\"JavaScript\"></script>")
        self.ficHTML.ecris("<link type=\"text/css\" rel=\"stylesheet\" href=\"/css/xtree.css\">")
        self.ficHTML.ecris("<style>")
        self.ficHTML.ecris("   body { background: white; color: black; }")
        self.ficHTML.ecris("   input { width: 120px; }")
        self.ficHTML.ecris("</style>")

    def xeq(self, tree):
        self.ficHTML = EWFichierHTML()
        self.ficHTML.ecrisTetePage("Arbre de dépendance inverse")
        self.writeProlog()
        self.ficHTML.ecris("<script language=\"JavaScript\">")
        self.ficHTML.ecris("if (document.getElementById)")
        self.ficHTML.ecris("{")
        self.creatTreeItems(self, "Root", tree)
        self.ficHTML.ecris("")
        self.addNodeToDisplayTree("Root", tree)
        self.ficHTML.ecris("   document.write(treeRoot);")
        self.ficHTML.ecris("}")
        self.ficHTML.ecris("</script>")
        self.ficHTML.ecrisPiedPage()


class EWArbreDependanceDirecte:
    def __init__(self):
        self.ficHTML = None

    def createTreeItems(self, root, tree):
        nodeKeys = list(tree.mapNodes.keys())
        nodeKeys.sort()
        self.ficHTML.ecris("   var %s = new WebFXTree('%s');" % (root, root))
        for n in nodeKeys:
            self.ficHTML.ecris("   var %s = new WebFXTreeItem('%s');" % (n, n))

    def addNodeToDisplayTree(self, dtNode, node):
        nodeKeys = list(node.mapNodes.keys())
        nodeKeys.sort()
        for n in nodeKeys:
            self.ficHTML.ecris("   %s.add(%s);" % (dtNode, n))
            if (node.mapNodes[n] != None):
                self.addNodeToDisplayTree(n, node.mapNodes[n])

    def writeProlog(self):
        self.ficHTML.ecris("<script src=\"/js/xtree.js\" language=\"JavaScript\"></script>")
        self.ficHTML.ecris("<link type=\"text/css\" rel=\"stylesheet\" href=\"/css/xtree.css\">")
        self.ficHTML.ecris("<style>")
        self.ficHTML.ecris("   body { background: white; color: black; }")
        self.ficHTML.ecris("   input { width: 120px; }")
        self.ficHTML.ecris("</style>")

    def xeq(self, tree):
        self.ficHTML = EWFichierHTML()
        self.ficHTML.ecrisTetePage("Arbre de dépendance")
        self.writeProlog()
        self.ficHTML.ecris("<script language=\"JavaScript\">")
        self.ficHTML.ecris("if (document.getElementById)")
        self.ficHTML.ecris("{")
        self.creatTreeItems(self, "Root", tree)
        self.ficHTML.ecris("")
        self.addNodeToDisplayTree("Root", tree)
        self.ficHTML.ecris("   document.write(treeRoot);")
        self.ficHTML.ecris("}")
        self.ficHTML.ecris("</script>")
        self.ficHTML.ecrisPiedPage()


if __name__ == '__main__':
    path = "DT_20040225-142946.xml"
    tree = EW_Tree.createTreeFromXML(path)
    arbre = EWArbreDependanceDirecte()
    arbre.xeq(tree)

