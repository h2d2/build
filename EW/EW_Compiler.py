# -*- coding: UTF-8 -*-

import fileinput
import re
import sys

class EWAlgoCompiler_CountErrors:
    def __init__(self):
        self.cpl = None

    def configure(self, cmplr, typCmplr):
        self.cpl = ctrCompiler(cmplr, typCmplr)

    def __call__(self, cmplr, typCmplr, path):
        self.configure(cmplr, typCmplr)

        errors = 0
        warnings = 0
        for line in fileinput.FileInput(path):
            if (len(line) < 10): continue
            if (line[0] == " "): continue
            if (line[0:3] == "In "): continue
            if (line.find(self.cpl.spdexpWarning) > 0):
                if (self.cpl.regexpWarning.match(line)):
                    warnings = warnings + 1
            elif (line.find(self.cpl.spdexpError) > 0):
                if (self.cpl.regexpError.match(line)):
                    errors = errors + 1
        return errors, warnings


class EWCompiler:
    def __init__(self):
        self.spdexpError   = None
        self.regexpError   = None
        self.spdexpWarning = None
        self.regexpWarning = None

#########################################################################################
##
##
#########################################################################################
class EWCompiler_gcc_cpp(EWCompiler):
    def __init__(self):
        EWCompiler.__init__(self)
        drvErr = r"(([a-zA-Z]:[\\/]|[\\/])?)"
        drvWrn = r"(([eE]:[\\/]?)?)"       # Limit to e: drive for warnings
        filExp = r"([^<>:\"/\\|() ][^<>:\"/\\|()]*)"
        dirExp = r"(" + filExp + r"[\\/])"
        linExp = r"(:([0-9]+))"
        errExp = r"(: error:)"
        wrnExp = r"(: warning: (?!inline ))"
        xprErr = "^" + "(?P<file>" + drvErr + dirExp + "*" + filExp + ")" + "(?P<line>" + linExp + ")" + linExp + "{0,1}" + errExp
        xprWrn = "^" + "(?P<file>" + drvWrn + dirExp + "*" + filExp + ")" + "(?P<line>" + linExp + ")" + linExp + "{0,1}" + wrnExp
        self.regexpError   = re.compile(xprErr)
        self.regexpWarning = re.compile(xprWrn)
        self.spdexpError   = ": error:"
        self.spdexpWarning = ": warning:"

class EWCompiler_gcc_ftn_3(EWCompiler):
    def __init__(self):
        EWCompiler.__init__(self)
        drvErr = r"(([a-zA-Z]:[\\/]|[\\/])?)"
        drvWrn = r"(([eE]:[\\/]?)?)"       # Limit to e: drive for warnings
        filExp = r"([^<>:\"/\\|() ][^<>:\"/\\|()]*)"
        dirExp = r"(" + filExp + r"[\\/])"
        linExp = r"(:([0-9]+))"
        errExp = r"(:\s*$)"
        wrnExp = r"(: warning:)"
        xprErr = "^" + "(?P<file>" + drvErr + dirExp + "*" + filExp + ")" + "(?P<line>" + linExp + ")" + linExp + "{0,1}" + errExp
        xprWrn = "^" + "(?P<file>" + drvWrn + dirExp + "*" + filExp + ")" + "(?P<line>" + linExp + ")" + linExp + "{0,1}" + wrnExp
        self.regexpError   = re.compile(xprErr)
        self.regexpWarning = re.compile(xprWrn)
        self.spdexpError   = ": "
        self.spdexpWarning = ": warning:"

EWCompiler_gccx86_cpp_xx = EWCompiler_gcc_cpp
EWCompiler_gccx64_cpp_xx = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_xx = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_33 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_34 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_41 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_42 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_43 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_44 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_45 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_46 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_47 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_48 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_49 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_50 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_51 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_52 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_53 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_54 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_55 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_60 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_61 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_62 = EWCompiler_gcc_cpp
EWCompiler_gcc_cpp_63 = EWCompiler_gcc_cpp

EWCompiler_gccx86_ftn_xx = EWCompiler_gcc_ftn_3
EWCompiler_gccx64_ftn_xx = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_xx = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_33 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_34 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_41 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_42 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_43 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_44 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_45 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_46 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_47 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_48 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_49 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_50 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_51 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_52 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_53 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_54 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_55 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_60 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_61 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_62 = EWCompiler_gcc_ftn_3
EWCompiler_gcc_ftn_63 = EWCompiler_gcc_ftn_3

#########################################################################################
##
##
#########################################################################################
class EWCompiler_intel_cpp(EWCompiler):
    def __init__(self):
        EWCompiler.__init__(self)
        drvErr = r"(([a-zA-Z]:[\\/]|[\\/])?)"
        drvWrn = r"(([eE]:[\\/]?)?)"       # Limit to e: drive for warnings
        filExp = r"([^<>:\"/\\|() ][^<>:\"/\\|()]*)"
        dirExp = r"(" + filExp + r"[\\/])"
        linExp = r"(\([0-9]+\))"
        errExp = r"[ ]*:( Internal compiler)?( catastrophic)?( error[: ])"
        wrnExp = r"[ ]*:( warning )"
        xprErr = "^" + "(?P<file>" + drvErr + "?" + dirExp + "*" + filExp + ")" + "(?P<line>" + linExp + "{0,1}" + ")" + linExp + "{0,1}" + errExp
        xprWrn = "^" + "(?P<file>" + drvWrn + "?" + dirExp + "*" + filExp + ")" + "(?P<line>" + linExp + "{0,1}" + ")" + linExp + "{0,1}" + wrnExp
        self.regexpError   = re.compile(xprErr)
        self.regexpWarning = re.compile(xprWrn)
        self.spdexpError   = " error"
        self.spdexpWarning = ": warning "

EWCompiler_intel_cpp_90  = EWCompiler_intel_cpp
EWCompiler_intel_cpp_91  = EWCompiler_intel_cpp
EWCompiler_intel_cpp_110 = EWCompiler_intel_cpp
EWCompiler_intel_cpp_111 = EWCompiler_intel_cpp

EWCompiler_itlx64_cpp_90  = EWCompiler_intel_cpp
EWCompiler_itlx64_cpp_91  = EWCompiler_intel_cpp
EWCompiler_itlx64_cpp_110 = EWCompiler_intel_cpp
EWCompiler_itlx64_cpp_111 = EWCompiler_intel_cpp
EWCompiler_itlx64_cpp_120 = EWCompiler_intel_cpp
EWCompiler_itlx64_cpp_140 = EWCompiler_intel_cpp
EWCompiler_itlx64_cpp_150 = EWCompiler_intel_cpp
EWCompiler_itlx64_cpp_160 = EWCompiler_intel_cpp
EWCompiler_itlx64_cpp_170 = EWCompiler_intel_cpp
EWCompiler_itlx64_cpp_180 = EWCompiler_intel_cpp

EWCompiler_itlx86_cpp_90  = EWCompiler_intel_cpp
EWCompiler_itlx86_cpp_91  = EWCompiler_intel_cpp
EWCompiler_itlx86_cpp_110 = EWCompiler_intel_cpp
EWCompiler_itlx86_cpp_111 = EWCompiler_intel_cpp
EWCompiler_itlx86_cpp_120 = EWCompiler_intel_cpp
EWCompiler_itlx86_cpp_140 = EWCompiler_intel_cpp
EWCompiler_itlx86_cpp_150 = EWCompiler_intel_cpp
EWCompiler_itlx86_cpp_160 = EWCompiler_intel_cpp
EWCompiler_itlx86_cpp_170 = EWCompiler_intel_cpp
EWCompiler_itlx86_cpp_180 = EWCompiler_intel_cpp

EWCompiler_itli64_cpp_90  = EWCompiler_intel_cpp
EWCompiler_itli64_cpp_91  = EWCompiler_intel_cpp
EWCompiler_itli64_cpp_110 = EWCompiler_intel_cpp
EWCompiler_itli64_cpp_111 = EWCompiler_intel_cpp
EWCompiler_itli64_cpp_120 = EWCompiler_intel_cpp
EWCompiler_itli64_cpp_140 = EWCompiler_intel_cpp
EWCompiler_itli64_cpp_150 = EWCompiler_intel_cpp
EWCompiler_itli64_cpp_160 = EWCompiler_intel_cpp
EWCompiler_itli64_cpp_170 = EWCompiler_intel_cpp
EWCompiler_itli64_cpp_180 = EWCompiler_intel_cpp

class EWCompiler_intel_ftn(EWCompiler):
    def __init__(self):
        EWCompiler.__init__(self)
        drvErr = r"(([a-zA-Z]:[\\/]|[\\/])?)"
        drvWrn = r"(([eE]:[\\/]?)?)"       # Limit to e: drive for warnings
        filExp = r"([^<>:\"/\\|() ][^<>:\"/\\|()]*)"
        dirExp = r"(" + filExp + r"[\\/])"
        linExp = r"(\([0-9]+\))"
        errExp = r"[ ]*:( Error:)"
        wrnExp = r"[ ]*:( Warning:)"
        xprErr = "^" + "(?P<file>" + drvErr + "?" + dirExp + "*" + filExp + ")" + "(?P<line>" + linExp + "{0,1}" + ")" + linExp + "{0,1}" + errExp
        xprWrn = "^" + "(?P<file>" + drvWrn + "?" + dirExp + "*" + filExp + ")" + "(?P<line>" + linExp + "{0,1}" + ")" + linExp + "{0,1}" + wrnExp
        self.regexpError   = re.compile(xprErr)
        self.regexpWarning = re.compile(xprWrn)
        self.spdexpError   = ": Error:"
        self.spdexpWarning = ": Warning:"

EWCompiler_intel_ftn_90  = EWCompiler_intel_ftn
EWCompiler_intel_ftn_91  = EWCompiler_intel_ftn
EWCompiler_intel_ftn_110 = EWCompiler_intel_ftn
EWCompiler_intel_ftn_111 = EWCompiler_intel_ftn

EWCompiler_itlx64_ftn_90  = EWCompiler_intel_ftn
EWCompiler_itlx64_ftn_91  = EWCompiler_intel_ftn
EWCompiler_itlx64_ftn_110 = EWCompiler_intel_ftn
EWCompiler_itlx64_ftn_111 = EWCompiler_intel_ftn
EWCompiler_itlx64_ftn_120 = EWCompiler_intel_ftn
EWCompiler_itlx64_ftn_140 = EWCompiler_intel_ftn
EWCompiler_itlx64_ftn_150 = EWCompiler_intel_ftn
EWCompiler_itlx64_ftn_160 = EWCompiler_intel_ftn
EWCompiler_itlx64_ftn_170 = EWCompiler_intel_ftn
EWCompiler_itlx64_ftn_180 = EWCompiler_intel_ftn

EWCompiler_itlx86_ftn_90  = EWCompiler_intel_ftn
EWCompiler_itlx86_ftn_91  = EWCompiler_intel_ftn
EWCompiler_itlx86_ftn_110 = EWCompiler_intel_ftn
EWCompiler_itlx86_ftn_111 = EWCompiler_intel_ftn
EWCompiler_itlx86_ftn_120 = EWCompiler_intel_ftn
EWCompiler_itlx86_ftn_140 = EWCompiler_intel_ftn
EWCompiler_itlx86_ftn_150 = EWCompiler_intel_ftn
EWCompiler_itlx86_ftn_160 = EWCompiler_intel_ftn
EWCompiler_itlx86_ftn_170 = EWCompiler_intel_ftn
EWCompiler_itlx86_ftn_180 = EWCompiler_intel_ftn

EWCompiler_itli64_ftn_90  = EWCompiler_intel_ftn
EWCompiler_itli64_ftn_91  = EWCompiler_intel_ftn
EWCompiler_itli64_ftn_110 = EWCompiler_intel_ftn
EWCompiler_itli64_ftn_111 = EWCompiler_intel_ftn
EWCompiler_itli64_ftn_120 = EWCompiler_intel_ftn
EWCompiler_itli64_ftn_140 = EWCompiler_intel_ftn
EWCompiler_itli64_ftn_150 = EWCompiler_intel_ftn
EWCompiler_itli64_ftn_160 = EWCompiler_intel_ftn
EWCompiler_itli64_ftn_170 = EWCompiler_intel_ftn
EWCompiler_itli64_ftn_180 = EWCompiler_intel_ftn

EWCompiler_cmc_cpp_xx = EWCompiler_intel_cpp
EWCompiler_cmc_ftn_xx = EWCompiler_intel_ftn
EWCompiler_cmcitl_cpp_xx = EWCompiler_intel_cpp
EWCompiler_cmcitl_ftn_xx = EWCompiler_intel_ftn


#########################################################################################
##
##
#########################################################################################
class EWCompilerVC(EWCompiler):
    def __init__(self):
        EWCompiler.__init__(self)
        drvErr = r"(([a-zA-Z]:[\\/]|[\\/])?)"
        drvWrn = r"([eE]:[\\/]?)"       # Limit to e: drive for warnings
        filExp = r"([^<>:\"/\\|() ][^<>:\"/\\|()]*)"
        dirExp = r"(" + filExp + r"[\\/])"
        linExp = r"(\([0-9]+\))"
        errExp = r"[ ]*:( fatal)?( error )"
        wrnExp = r"[ ]*:( warning )"
        xprErr = "^" + "(?P<file>" + drvErr + "?" + dirExp + "*" + filExp + ")" + "(?P<line>" + linExp + "{0,1}" + ")" + linExp + "{0,1}" + errExp
        xprWrn = "^" + "(?P<file>" + drvWrn + "?" + dirExp + "*" + filExp + ")" + "(?P<line>" + linExp + "{0,1}" + ")" + linExp + "{0,1}" + wrnExp
        self.regexpError   = re.compile(xprErr)
        self.regexpWarning = re.compile(xprWrn)
        self.spdexpError   = " error "
        self.spdexpWarning = ": warning "


#########################################################################################
##
##
#########################################################################################
class EWCompiler_sun_cpp(EWCompiler):
    def __init__(self):
        EWCompiler.__init__(self)
        drvErr = r"(([a-zA-Z]:[\\/]|[\\/])?)"
        drvWrn = r"(([eE]:[\\/]?)?)"       # Limit to e: drive for warnings
        filExp = r"([^<>:\"/\\|() ][^<>:\"/\\|()]*)"
        dirExp = r"(" + filExp + r"[\\/])"
        linExp = r"(\([0-9]+\))"
        errExp = r"[ ]*:( Internal compiler)?( catastrophic)?( error[: ])"
        wrnExp = r"[ ]*:( warning )"
        xprErr = "^" + "(?P<file>" + drvErr + "?" + dirExp + "*" + filExp + ")" + "(?P<line>" + linExp + "{0,1}" + ")" + linExp + "{0,1}" + errExp
        xprWrn = "^" + "(?P<file>" + drvWrn + "?" + dirExp + "*" + filExp + ")" + "(?P<line>" + linExp + "{0,1}" + ")" + linExp + "{0,1}" + wrnExp
        self.regexpError   = re.compile(xprErr)
        self.regexpWarning = re.compile(xprWrn)
        self.spdexpError   = " error"
        self.spdexpWarning = ": warning "

EWCompiler_sun_cpp_120 = EWCompiler_sun_cpp
EWCompiler_sun_cpp_121 = EWCompiler_sun_cpp
EWCompiler_sun_cpp_122 = EWCompiler_sun_cpp
EWCompiler_sun_cpp_123 = EWCompiler_sun_cpp
EWCompiler_sun_cpp_125 = EWCompiler_sun_cpp
EWCompiler_sun_cpp_126 = EWCompiler_sun_cpp

class EWCompiler_sun_ftn(EWCompiler):
    def __init__(self):
        EWCompiler.__init__(self)
        drvErr = r"(([a-zA-Z]:[\\/]|[\\/])?)"
        drvWrn = r"(([eE]:[\\/]?)?)"       # Limit to e: drive for warnings
        filExp = r"([^<>:\"/\\|() ][^<>:\"/\\|()]*)"
        dirExp = r"(" + filExp + r"[\\/])"
        linExp = r"(, Line = [0-9]+, Column = [0-9]+)"
        errExp = r"[ ]*:( ERROR:)"
        wrnExp = r"[ ]*:( WARNING:)"
        xprErr = "^" + r'"' + "(?P<file>" + drvErr + "?" + dirExp + "*" + filExp + ")" + r'"' + "(?P<line>" + linExp + "{0,1}" + ")" + linExp + "{0,1}" + errExp
        xprWrn = "^" + r'"' + "(?P<file>" + drvWrn + "?" + dirExp + "*" + filExp + ")" + r'"' + "(?P<line>" + linExp + "{0,1}" + ")" + wrnExp
        self.regexpError   = re.compile(xprErr)
        self.regexpWarning = re.compile(xprWrn)
        self.spdexpError   = ": ERROR:"
        self.spdexpWarning = ": WARNING:"

EWCompiler_sun_ftn_120 = EWCompiler_sun_ftn
EWCompiler_sun_ftn_121 = EWCompiler_sun_ftn
EWCompiler_sun_ftn_122 = EWCompiler_sun_ftn
EWCompiler_sun_ftn_123 = EWCompiler_sun_ftn
EWCompiler_sun_ftn_125 = EWCompiler_sun_ftn
EWCompiler_sun_ftn_126 = EWCompiler_sun_ftn

#########################################################################################
##
##
#########################################################################################
class EWCompiler_open64_cpp(EWCompiler):
    def __init__(self):
        EWCompiler.__init__(self)
        drvErr = r"(([a-zA-Z]:[\\/]|[\\/])?)"
        drvWrn = r"(([eE]:[\\/]?)?)"       # Limit to e: drive for warnings
        filExp = r"([^<>:\"/\\|() ][^<>:\"/\\|()]*)"
        dirExp = r"(" + filExp + r"[\\/])"
        linExp = r"(\([0-9]+\))"
        errExp = r"[ ]*:( Internal compiler)?( catastrophic)?( error[: ])"
        wrnExp = r"[ ]*:( warning )"
        xprErr = "^" + "(?P<file>" + drvErr + "?" + dirExp + "*" + filExp + ")" + "(?P<line>" + linExp + "{0,1}" + ")" + linExp + "{0,1}" + errExp
        xprWrn = "^" + "(?P<file>" + drvWrn + "?" + dirExp + "*" + filExp + ")" + "(?P<line>" + linExp + "{0,1}" + ")" + linExp + "{0,1}" + wrnExp
        self.regexpError   = re.compile(xprErr)
        self.regexpWarning = re.compile(xprWrn)
        self.spdexpError   = " error"
        self.spdexpWarning = ": warning "

EWCompiler_open64_cpp_xx = EWCompiler_open64_cpp

class EWCompiler_open64_ftn(EWCompiler):
    def __init__(self):
        EWCompiler.__init__(self)
        drvErr = r"(([a-zA-Z]:[\\/]|[\\/])?)"
        drvWrn = r"(([eE]:[\\/]?)?)"       # Limit to e: drive for warnings
        filExp = r"([^<>:\"/\\|() ][^<>:\"/\\|()]*)"
        dirExp = r"(" + filExp + r"[\\/])"
        linExp = r"(, Line = [0-9]+, Column = [0-9]+)"
        errExp = r"[ ]*:( Error:)"
        wrnExp = r"[ ]*:( WARNING:)"
        xprErr = "^" + r'"' + "(?P<file>" + drvErr + "?" + dirExp + "*" + filExp + ")" + r'"' + "(?P<line>" + linExp + "{0,1}" + ")" + linExp + "{0,1}" + errExp
        xprWrn = "^" + r'"' + "(?P<file>" + drvWrn + "?" + dirExp + "*" + filExp + ")" + r'"' + "(?P<line>" + linExp + "{0,1}" + ")" + wrnExp
        self.regexpError   = re.compile(xprErr)
        self.regexpWarning = re.compile(xprWrn)
        self.spdexpError   = ": Error:"
        self.spdexpWarning = ": WARNING:"

EWCompiler_open64_ftn_xx = EWCompiler_open64_ftn


#########################################################################################
##
##
#########################################################################################
def ctrCompiler(nom, typ = 'cpp'):
    ext2typ = { 'c'  : 'c', 'C'  : 'c',
                'cpp': 'cpp', 'CPP': 'cpp', 'cc' : 'cpp', 'CC' : 'cpp',
                'f'  : 'ftn', 'F'  : 'ftn', 'for': 'ftn', 'FOR': 'ftn', 'ftn': 'ftn', 'FTN': 'ftn'}

    nom = nom.lower()
    cplrNam = ''
    try:
        gen, ver = nom.split('-')
        if (ver != 'xx'): ver = ver.replace('.', '')
        cplrNam = 'EWCompiler_%s_%s_%s' % (gen, ext2typ[typ], ver)
    except (ValueError, KeyError) as e:
        raise NotImplementedError("ERR - Unsupported compiler: name=%s type=%s" % (nom, typ) )
    try:
        if __name__ == '__main__':
            return sys.modules['__main__'].__dict__[cplrNam]()
        else:
            return sys.modules['EW_Compiler'].__dict__[cplrNam]()
    except:
        raise NotImplementedError("ERR - Unsupported compiler: name=%s type=%s (%s)" % (nom, typ, cplrNam) )


if __name__ == '__main__':
    algo = EWAlgoCompiler_CountErrors()
    errors, warnings = algo("gcc-3.4", "for", r"E:\dev\_tst\H2D2\derniere\libh2d2\win32\gcc-3.4\debug\source\nmprxy.for.err")
    print(errors, warnings)
